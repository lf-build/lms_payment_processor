﻿using LMS.Payment.Processor.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Date;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Services;

namespace LMS.Payment.Processor
{
    public class PaymentAttemptsRepository : MongoRepository<ILoanPaymentAttempts, LoanFundingRequestAttempts>, IPaymentAttemptsRepository
    {
        static PaymentAttemptsRepository()
        {

            BsonClassMap.RegisterClassMap<LoanFundingRequestAttempts>(map =>
            {
                map.AutoMap();
                //  map.MapProperty(m => m.ReturnDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                //  map.MapProperty(m => m.VendorResponseDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(LoanFundingRequestAttempts);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<PaymentRequestAttempts>(map =>
            {
                map.AutoMap();
                var type = typeof(PaymentRequestAttempts);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public PaymentAttemptsRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "PaymentAttempts")
        {
            CreateIndexIfNotExists("FundingAttempts_FundingAttemptsId", Builders<ILoanPaymentAttempts>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task<ILoanPaymentAttempts> UpdateStatus(string referenceId, IUpdateFundingAttemptStatusRequest updateRequest)
        {
            var record = Query.Where(i => i.ReferenceId == referenceId);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with reference number {referenceId} is not found");
            var attamptId = record.OrderByDescending(f => f.AttemptDate).First().FundingRequestAttemptId;
            await Collection.UpdateOneAsync(Builders<ILoanPaymentAttempts>.Filter.Where((a => a.TenantId == TenantService.Current.Id && a.FundingRequestAttemptId == attamptId)),
            Builders<ILoanPaymentAttempts>.Update.Set(a => a.ProviderResponseStatus, updateRequest.Status)
            .Set(a => a.ProviderResponseDate, new TimeBucket(updateRequest.ResponseDate))
            .Set(a => a.ReturnCode, updateRequest.ReturnCode)
            .Set(a => a.ReturnReason, updateRequest.ReturnReason)
            .Set(a => a.IsAccounted, updateRequest.IsAccounted)
            .Set(a => a.AttemptStatus, updateRequest.Status));

            record.First().ProviderResponseStatus = updateRequest.Status;
            record.First().ProviderResponseDate = new TimeBucket(updateRequest.ResponseDate);
            record.First().ReturnCode = updateRequest.ReturnCode;
            record.First().AttemptStatus = updateRequest.Status;
            record.First().IsAccounted = updateRequest.IsAccounted;
            return record.First();
        }
        public async Task<List<ILoanPaymentAttempts>> GetAllAttemptData(string loanNumber)
        {
            var records = await Query.Where(i => i.LoanNumber == loanNumber).OrderByDescending(x => x.AttemptDate).ToListAsync();
            return records;
        }
        public async Task<List<ILoanPaymentAttempts>> GetAttemptData(string referenceId)
        {
            var records = await Query.Where(i => i.ReferenceId == referenceId).ToListAsync();
            return records;
        }
        public async Task<ILoanPaymentAttempts> GetAttemptData(string referenceId, string providerNumber)
        {
            var records = await Query.FirstOrDefaultAsync(i => i.ReferenceId == referenceId && i.ProviderReferenceNumber == providerNumber);
            return records;
        }

        private FilterDefinition<ILoanPaymentAttempts> GetByApplicationNumberFilter(string referenceId)
        {
            return Builders<ILoanPaymentAttempts>.Filter.Where(a => a.TenantId == TenantService.Current.Id
                                                                          && a.ReferenceId == referenceId);
        }

        public async Task<List<ILoanPaymentAttempts>> GetPaymentAttempts()
        {
            var records = await Query.ToListAsync();
            return records;
        }

        public async Task<List<ILoanPaymentAttempts>> GetAttempts(AttemptRequest attemptDate)
        {
            var records = await Query.Where(i => i.AttemptDate.Time >= attemptDate.FromDate.Date && i.AttemptDate.Time <= attemptDate.ToDate.Date).OrderByDescending(x => x.LoanNumber).ToListAsync();
            return records;
        }

         public async Task<List<ILoanPaymentAttempts>> GetbyReturnDate(AttemptRequest attemptDate)
        {
            var records = await Query.Where(i =>i.ReturnDate != null && i.ReturnDate.Time >= attemptDate.FromDate.Date && i.ReturnDate.Time <= attemptDate.ToDate.Date).OrderByDescending(x => x.LoanNumber).ToListAsync();
            return records;
        }
        
          public async Task<List<ILoanPaymentAttempts>> GetReSubmitAttempts(AttemptRequest attemptDate)
        {
            var records = await Query.Where(i => i.AttemptDate.Time >= attemptDate.FromDate.Date && i.AttemptDate.Time <= attemptDate.ToDate.Date).OrderByDescending(x => x.LoanNumber).ToListAsync();
            
            var reAttemptedData = records.Where(x =>x.AttemptCount > 1).ToList();
            var failedData = records.Where(x =>x.AttemptCount == 1 && x.AttemptStatus == "failed").ToList();
            reAttemptedData.AddRange(failedData);

            return reAttemptedData;
        }

    }
}