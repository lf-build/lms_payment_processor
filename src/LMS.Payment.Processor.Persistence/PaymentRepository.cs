﻿using LMS.Payment.Processor.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Date;
using LMS.LoanManagement.Abstractions;
using LMS.LoanAccounting;
using LendFoundry.Foundation.Services;

namespace LMS.Payment.Processor
{
    public class PaymentRepository : MongoRepository<IFunding, Funding>, Persistence.IPaymentRepository
    {
        static PaymentRepository()
        {
            BsonClassMap.RegisterClassMap<Funding>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.PaymentInstrumentType).SetSerializer(new EnumSerializer<PaymentInstrumentType>(BsonType.String));
                map.MapProperty(p => p.PaymentType).SetSerializer(new EnumSerializer<PaymentType>(BsonType.String));
                map.MapProperty(p => p.PaymentRail).SetSerializer(new EnumSerializer<PaymentRail>(BsonType.String));
                map.MapProperty(p => p.TransactionType).SetSerializer(new EnumSerializer<TransactionType>(BsonType.String));
                map.MapProperty(p => p.ResubmitType).SetSerializer(new EnumSerializer<ResubmitType>(BsonType.String));


                var type = typeof(Funding);

                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<FundingRequest>(map =>
            {
                map.AutoMap();
                var type = typeof(FundingRequest);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<FeeDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(FeeDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

        }

        public PaymentRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "payment-pull")
        {
            CreateIndexIfNotExists("payment-pullId", Builders<IFunding>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
            CreateIndexIfNotExists("ref-id", Builders<IFunding>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.ReferenceId));
            CreateIndexIfNotExists("payment-ref-id", Builders<IFunding>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending(i => i.ReferenceId));

        }

        public async Task<IEnumerable<IFunding>> GetFundingRequest(IEnumerable<string> statuses)
        {
            return await Query.Where(x => statuses.Contains(x.RequestStatus)).ToListAsync();
        }

        public async Task<List<IFunding>> GetFundingRequest(string entityType, string entityId)
        {
            return await Query.Where(x => x.EntityId == entityId && x.EntityType == entityType).ToListAsync();
        }

        public async Task<IFunding> GetFundingByReferenceId(string referenceId)
        {
            return await Query.FirstOrDefaultAsync(x => x.ReferenceId == referenceId);
        }
        public async Task<IFunding> UpdateNextRetryDate(string entityType, string entityId, string referenceId, DateTimeOffset nextRetryDate)
        {
            var record = Query.Where(i => i.ReferenceId == referenceId);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with reference number {referenceId} is not found");

            await Collection.UpdateOneAsync(Builders<IFunding>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.ReferenceId == referenceId),
            Builders<IFunding>.Update.Set(a => a.NextRetryDate, new TimeBucket(nextRetryDate)));
            record.First().NextRetryDate = new TimeBucket(nextRetryDate);
            return record.First();
        }

        public async Task<IFunding> UpdateFundingStatus(string entityType, string entityId, DateTimeOffset statusUpdatedDate, IUpdateFundingStatus updateRequest)
        {
            var record = Query.Where(i => i.ReferenceId == updateRequest.ReferenceId);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with reference number {updateRequest.ReferenceId} is not found");

            await Collection.UpdateOneAsync(Builders<IFunding>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.ReferenceId == updateRequest.ReferenceId),
            Builders<IFunding>.Update.Set(a => a.RequestStatus, updateRequest.status));
            record.First().RequestStatus = updateRequest.status;
            record.First().StatusUpdatedDate = new TimeBucket(statusUpdatedDate);
            return record.First();
        }
        public async Task<IFunding> UpdateRetryCount(string entityType, string entityId, IUpdateRetryCount updateRequest)
        {
            var record = Query.Where(i => i.ReferenceId == updateRequest.ReferenceId);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with reference number {updateRequest.ReferenceId} is not found");

            await Collection.UpdateOneAsync(Builders<IFunding>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.ReferenceId == updateRequest.ReferenceId),
            Builders<IFunding>.Update.Set(a => a.RetryCount, updateRequest.Count));
            record.First().RetryCount = updateRequest.Count;
            return record.First();
        }

        public async Task<IFunding> UpdateFunding(IFunding updateRequest)
        {

            var record = Query.FirstOrDefault(i => i.Id == updateRequest.Id);
            if (record == null)
                throw new NotFoundException($"Record with funding id {updateRequest.Id} is not found");

            record.PaymentAmount = updateRequest.PaymentAmount;
            record.BankAccountNumber = updateRequest.BankAccountNumber;
            record.BankAccountType = updateRequest.BankAccountType;
            record.BankRTN = updateRequest.BankRTN;
            record.BorrowersName = updateRequest.BorrowersName;
            record.EntityId = updateRequest.EntityId;
            record.EntityType = updateRequest.EntityType;
            record.LoanNumber = updateRequest.LoanNumber;
            record.ReferenceId = updateRequest.ReferenceId;
            record.RequestedDate = updateRequest.RequestedDate;
            record.RequestStatus = updateRequest.RequestStatus;
            record.RetryCount = updateRequest.RetryCount;
            record.NextRetryDate = updateRequest.NextRetryDate;
            record.InterestAmount = updateRequest.InterestAmount;
            record.PrincipalAmount = updateRequest.PrincipalAmount;
            record.AdditionalInterestAmount = updateRequest.AdditionalInterestAmount;
            record.Fees = updateRequest.Fees;
            record.IsFileCreated = updateRequest.IsFileCreated;
            record.IsReSubmitted = updateRequest.IsReSubmitted;
            record.IsAccounted = updateRequest.IsAccounted;
            record.FileCreatedDate = updateRequest.FileCreatedDate;
            record.PaymentAccountDate = updateRequest.PaymentAccountDate;
            record.ResubmitType = updateRequest.ResubmitType;
            await Collection.ReplaceOneAsync(Builders<IFunding>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.Id == updateRequest.Id), record);

            return record;
        }

        public async Task<List<IFunding>> GetFundingRequestsByDate(DateTime startDate, DateTime? endDate)
        {
            List<IFunding> fundinginfos = new List<IFunding>();
            var details = await Query.ToListAsync();
            if (endDate == null)
            {
                endDate = startDate;
            }
            foreach (var item in details)
            {
                if (item.RequestedDate.Time.Date >= startDate && item.RequestedDate.Time.Date <= endDate)
                    fundinginfos.Add(item);
            }

            return fundinginfos;
        }
        public async Task<List<IFunding>> GetFundingAccountedByDate(DateTime startDate, DateTime? endDate)
        {
            List<IFunding> fundinginfos = new List<IFunding>();
            var details = await Query.ToListAsync();
            if (endDate == null)
            {
                endDate = startDate;
            }
            foreach (var item in details)
            {
                if (item.ActualAccountDate != null)
                {
                    if (item.ActualAccountDate.Time.Date >= startDate && item.ActualAccountDate.Time.Date <= endDate)
                        fundinginfos.Add(item);
                }
            }

            return fundinginfos;
        }

        public async Task<List<IFunding>> GetFundingAttemptsByReferenceIds(List<string> referenceNumbers)
        {
            return await Query.Where(info => referenceNumbers.Contains(info.ReferenceId)).ToListAsync();

        }

    }
}