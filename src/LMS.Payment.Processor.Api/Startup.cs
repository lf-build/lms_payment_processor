﻿using LMS.Payment.Processor.Persistence;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Calendar.Client;
using LMS.LoanManagement.Client;
using LMS.LoanAccounting.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.ProductConfiguration.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LMS.Loan.Filters.Client;
using LendFoundry.Flag.Client;

namespace LMS.Payment.Processor.Api
{
    // public class Startup
    internal class Startup
    {
        public Startup(IHostingEnvironment env)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "PaymentProcessor"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LMS.Payment.Processor.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // services.AddMongoConfiguration(Settings.ServiceName);
#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<FundingConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddLookupService();
            services.AddCalendarService();
            services.AddLoanManagementService();
            services.AddAccrualBalance();
            services.AddNumberGeneratorService();
            services.AddAchService();
            services.AddProductService();
            services.AddStatusManagementService();
            services.AddLoanFilterService();
            services.AddFlag();
            services.AddDependencyServiceUriResolver<FundingConfiguration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            // services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<FundingConfiguration>>().Get());
            services.AddTransient<IPaymentAttemptsRepository, PaymentAttemptsRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IPaymentRepositoryFactory, PaymentRepositoryFactory>();
            services.AddTransient<IPaymentAttemptsRepositoryFactory, PaymentAttemptsRepositoryFactory>();
            services.AddTransient<IPaymentProcessorService, PaymentProcessorService>();
            services.AddTransient<IPaymentProcessorListener, PaymentProcessorListener>();
            services.AddTransient<IPaymentProcessorServiceFactory, PaymentProcessorServiceFactory>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
            app.UseCors(env);
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Payment Processor");
            });
#else
            app.UseSwaggerDocumentation();
#endif    
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UsePaymentProcessorListener();
            app.UseMvc();
        }
    }
}
