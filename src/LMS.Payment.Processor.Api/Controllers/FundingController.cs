﻿using LendFoundry.Foundation.Services;
using LMS.LoanAccounting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;

namespace LMS.Payment.Processor.Api.Controllers
{
    /// <summary>
    /// FundingController
    /// </summary>
    [Route("/")]
    public class FundingController : ExtendedController
    {
        #region Constructors

        /// <summary>
        /// FundingController constructor with logger
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public FundingController(IPaymentProcessorService service, ILogger logger) :base (logger)
        {
            FundingService = service;
        }

        #endregion Constructors

        #region Private Properties
        private NoContentResult NoContentResult { get; } = new NoContentResult();

        private IPaymentProcessorService FundingService { get; }

        #endregion Private Properties

        #region Write APIs
        /// <summary>
        /// Add Attempted Payments
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        [HttpPost("{entityType}/{entityId}/addfundingattempt")]
        [ProducesResponseType(typeof(ILoanPaymentAttempts), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddAttempts(string entityType, string entityId,
            [FromBody]PaymentRequestAttempts request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.AddFundingRequestAttempts(entityType, entityId, request));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, request});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Add Funding Request
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        [HttpPost("/{entityType}/{entityId}/add")]
        [ProducesResponseType(typeof(IFunding), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddFundingRequest(string entityType, string entityId,
            [FromBody]FundingRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.AddFundingRequest(entityType, entityId, request));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, request});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Update Funding Request
        /// </summary>
        /// <param name="request"></param>
        [HttpPost("/update/funding")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateFundingData([FromBody]Funding request)
        {
            return Execute(() =>
            {
                try
                {
                    FundingService.UpdateFundingData(request);
                    return NoContentResult;
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {request});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });

        }

        /// <summary>
        /// Add manual payments
        /// This method returns a list of payment objects as opposed to AddManualPayment
        /// In future only this API will be supported (/manual/payments) and the old one (/manual/payment) will be out.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="portfolioTypes">A comma separated list of portfolio types for which payment is made</param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPost("/{entityType}/{entityId}/manual/payments")]
        [ProducesResponseType(typeof(IPayment), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddManualPayments(string entityType, string entityId,
            [FromQuery(Name = "portfolio-types")] string portfolioTypes, [FromBody] FundingRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (entityId == null)
                    {
                        throw new ArgumentNullException(nameof(entityId));
                    }

                    var loanPortfolioTypes = new List<string>();
                    if (!string.IsNullOrWhiteSpace(portfolioTypes))
                    {
                        loanPortfolioTypes.AddRange(portfolioTypes.Split(','));
                    }

                    return Ok(await FundingService.AddManualPayments(entityType, entityId,
                        request, loanPortfolioTypes));
                }
                catch (NotFoundException notFoundException)
                {
                    Logger.Warn(FlattenException(notFoundException), new {entityType, entityId, portfolioTypes, request});
                    return ErrorResult.NotFound(notFoundException.Message);
                    
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, portfolioTypes, request});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
        
        /// <summary>
        /// Add manual payment
        /// This will be deprecated in favor of AddManualPayments, which will return a list
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="portfolioTypes">A comma separated list of portfolio types for which payment is made</param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPost("/{entityType}/{entityId}/manual/payment")]
        [ProducesResponseType(typeof(IPayment), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddManualPayment(string entityType, string entityId,
            [FromQuery(Name = "portfolio-types")]string portfolioTypes, [FromBody]FundingRequest request)
        {

            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (entityId == null)
                    {
                        throw new ArgumentNullException(nameof(entityId));
                    }

                    var loanPortfolioTypes = new List<string>();
                    if (!string.IsNullOrWhiteSpace(portfolioTypes))
                    {
                        loanPortfolioTypes.AddRange(portfolioTypes.Split(','));
                    }

                    return Ok(await FundingService.AddManualPayment(entityType, entityId,
                        request, loanPortfolioTypes));
                }
                catch (NotFoundException notFoundException)
                {
                    Logger.Warn(FlattenException(notFoundException), new {entityType, entityId, portfolioTypes, request});
                    return ErrorResult.NotFound(notFoundException.Message);
                    
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, portfolioTypes, request});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Resubmit Payment
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        [HttpPost("{entityType}/{entityId}/resubmit")]
        [ProducesResponseType(typeof(IFunding), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ReSubmitPayment(string entityType, string entityId, [FromBody]ReSubmitPaymentRequest request)
        {
            if (entityId == null)
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.ReSubmitPayment(entityType, entityId, request));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, request});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Update Funding Status
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="updateRequest"></param>
        [HttpPost("/{entityType}/{entityId}/fundingstatus")]
        [ProducesResponseType(typeof(IFunding), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateFundingStatus(string entityType, string entityId, [FromBody]UpdateFundingStatus updateRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.UpdateFundingStatus(entityType, entityId, updateRequest));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, updateRequest});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Update Retry Count
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="updateRequest"></param>
        [HttpPost("/{entityType}/{entityId}/retrycount")]
        [ProducesResponseType(typeof(IFunding), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateRetryCount(string entityType, string entityId, [FromBody]UpdateRetryCount updateRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.UpdateRetryCount(entityType, entityId, updateRequest));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, updateRequest});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Update Next Retry Date
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="updateRequest"></param>
        [HttpPost("/{entityType}/{entityId}/retrydate")]
        [ProducesResponseType(typeof(IFunding), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateNextRetryDate(string entityType, string entityId,
            [FromBody]UpdateRetryCount updateRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.UpdateNextRetryDate(entityType, entityId,
                        updateRequest));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, updateRequest});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Update Funding Attempt Status
        /// POST {referenceid}/attamptstatus will be deprecated in favor of POST {referenceid}/attemptstatus
        /// which has the correct spelling of attempt
        /// </summary>
        /// <param name="referenceid"></param>
        /// <param name="updateRequest"></param>
        [HttpPost("{referenceid}/attamptstatus")] // will be deprecated
        [HttpPost("{referenceid}/attemptstatus")]
        [ProducesResponseType(typeof(ILoanPaymentAttempts), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateFundingAttemptStatus(string referenceid,
            [FromBody]UpdateFundingAttemptStatusRequest updateRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.UpdateAttemptStatus(referenceid, updateRequest));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {referenceid, updateRequest});
                    return ErrorResult.BadRequest(ex.Message);
                }

            });
        }

        /// <summary>
        /// Refund Payment
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        [HttpPost("/{entityType}/{entityId}/refund/payment")]
        [ProducesResponseType(typeof(IFunding), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> RefundPayment(string entityType, string entityId,
            [FromBody]FundingRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.MakeRefundPayment(entityType, entityId, request));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId, request});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        #endregion Write APIs
        
        #region Read APIs
        /// <summary>
        /// Get InProgress Payments
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        [HttpGet("{entityType}/{entityId}/inprogress")]
        [ProducesResponseType(typeof(List<IFunding>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetInProgressPayments(string entityType, string entityId)
        {
            if (entityId == null)
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.GetInProgressPayments(entityType, entityId));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {entityType, entityId});
                    return ErrorResult.BadRequest(ex.Message);
                    
                }
            });
        }
        
        /// <summary>
        /// Get Funding Request by status
        /// </summary>
        /// <param name="statuses"></param>
        [HttpGet("status/{*statuses}")]
        [ProducesResponseType(typeof(IEnumerable<IFunding>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingRequest(string statuses)
        {
            if (string.IsNullOrWhiteSpace(statuses))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(statuses));
            }

            statuses = WebUtility.UrlDecode(statuses);
            var listStatus = SplitStatus(statuses);
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await FundingService.GetFundingRequest(listStatus));
                }
                catch (Exception ex)
                {
                    Logger.Error(FlattenException(ex), new {statuses});
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
        
        /// <summary>
        /// Get Funding Request by Loan number
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        [HttpGet("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(List<IFunding>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingData(string entityType, string entityId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetFundingData(entityType, entityId)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {entityType, entityId});
                return ErrorResult.BadRequest(ex.Message);
            }
        }
        
        /// <summary>
        /// Get Funding Request by ReferenceId
        /// </summary>
        /// <param name="referenceId"></param>
        [HttpGet("/{referenceId}")]
        [ProducesResponseType(typeof(IFunding), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingByReferenceId(string referenceId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetFundingByReferenceId(referenceId)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {referenceId});
                return ErrorResult.BadRequest(ex.Message);
            }
        }
        
        /// <summary>
        /// Get Funding Attempt Data by ReferenceId
        /// </summary>
        /// <param name="referenceId"></param>
        [HttpGet("fundingattempt/{referenceId}")]
        [ProducesResponseType(typeof(List<ILoanPaymentAttempts>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingAttemptData(string referenceId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetAttemptData(referenceId)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {referenceId});
                return ErrorResult.BadRequest(ex.Message);
            }
        }
        
        /// <summary>
        /// Get Payment Pull Request By Date
        /// </summary>
        /// <param name="searchdata"></param>
        [HttpPost("/paymentpull/date")]
        [ProducesResponseType(typeof(List<IFunding>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPaymentPullByDate([FromBody]SearchRequest searchdata)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await (FundingService.GetFundingRequestsByDate(searchdata))));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {searchdata});
                return ErrorResult.BadRequest(ex.Message);
            }
        }
        
        /// <summary>
        /// Get Payment Accounted By Date
        /// </summary>
        /// <param name="searchdata"></param>
        [HttpPost("/paymentaccounted/date")]
        [ProducesResponseType(typeof(List<IFunding>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPaymentAccountedByDate([FromBody]SearchRequest searchdata)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetFundingAccountedByDate(searchdata)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {searchdata});
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get All Payment Attempts
        /// </summary>
        [HttpGet("/paymentattept/all")]
        [ProducesResponseType(typeof(List<ILoanPaymentAttempts>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPaymentAttempts()
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetPaymentAttempts()));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex));
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Funding Attempts By ReferenceIds
        /// </summary>
        /// <param name="paymentDetailsRequest"></param>
        [HttpPost("/fundingattempt")]
        [ProducesResponseType(typeof(List<IFunding>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingAttemptsByReferenceIds([FromBody]PaymentDetailsRequest
            paymentDetailsRequest)
        {
            try
            {
                return await ExecuteAsync(async () =>
                    Ok(await (FundingService.GetFundingAttemptsByReferenceIds(paymentDetailsRequest))));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {paymentDetailsRequest});
                return ErrorResult.BadRequest(ex.Message);
            }
        }
        
        /// <summary>
        /// Get Attempts
        /// </summary>
        /// <param name="attemptRequest"></param>
        [HttpPost("/attempt/report")]
        [ProducesResponseType(typeof(List<AchDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAttempts([FromBody]AttemptRequest attemptRequest)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetAttempts(attemptRequest)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {attemptRequest});
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Loan Earning Report
        /// </summary>
        /// <param name="attemptRequest"></param>
        [HttpPost("/earning/report")]
        [ProducesResponseType(typeof(List<LoanEarningDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetEarningReport([FromBody]LoanEarningRequest attemptRequest)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetEarningReport(attemptRequest)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {attemptRequest});
                return ErrorResult.BadRequest(ex.Message);
            }
        }
 
        /// <summary>
        /// Get ACH Failure Report
        /// </summary>
        /// <param name="attemptRequest"></param>
        [HttpPost("/failure/report")]
        [ProducesResponseType(typeof(List<AchDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetACHFailureReport([FromBody]AttemptRequest attemptRequest)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await (FundingService.GetACHFailureReport(attemptRequest))));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {attemptRequest});
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Payment Report
        /// </summary>
        /// <param name="attemptRequest"></param>
        [HttpPost("/payment/report")]
        [ProducesResponseType(typeof(List<AchDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPaymentReport([FromBody]AttemptRequest attemptRequest)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetPaymentReport(attemptRequest)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {attemptRequest});
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get TDS Report
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="tagName"></param>
        [HttpGet("{entityType}/{tagName}/tds/report")]
        [ProducesResponseType(typeof(List<TDSReportDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTDSReport(string entityType,string tagName)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await FundingService.GetTDSReport(entityType,tagName)));
            }
            catch (Exception ex)
            {
                Logger.Error(FlattenException(ex), new {entityType,tagName});
                return ErrorResult.BadRequest(ex.Message);
            }
        }
    
    #endregion Read APIs
        
    #region Private Methods
        
        /// <summary>
        /// SplitStatus
        /// </summary>
        /// <param name="statuses"></param>
        private static List<string> SplitStatus(string statuses)
        {
            return string.IsNullOrWhiteSpace(statuses)
                ? new List<string>()
                : statuses.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
        
        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    #endregion Private Methods
    }
}