﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Configuration;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.Calendar.Client;
using LMS.LoanManagement.Client;
using LMS.LoanAccounting;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.StatusManagement.Client;
using LendFoundry.ProductConfiguration.Client;
using LMS.Loan.Filters.Client;
using LendFoundry.Flag.Client;

namespace LMS.Payment.Processor
{
    public class PaymentProcessorServiceFactory : IPaymentProcessorServiceFactory
    {

        public PaymentProcessorServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
            
        public IPaymentProcessorService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {

            var fundingRepositoryFactory = Provider.GetService<IPaymentRepositoryFactory>();
            var fundingRepository = fundingRepositoryFactory.Create(reader);

            var fundingRequestAttemptsRepositoryFactory = Provider.GetService<IPaymentAttemptsRepositoryFactory>();
            var fundingRequestAttemptsRepository = fundingRequestAttemptsRepositoryFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTimeService = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var numberGeneratorServiceFactory = Provider.GetService<INumberGeneratorServiceFactory>();
            var numberGeneratorService = numberGeneratorServiceFactory.Create(reader);

            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var loggerService = loggerFactory.CreateLogger();

            var lookUpFactory = Provider.GetService<ILookupClientFactory>();
            var lookUpService = lookUpFactory.Create(reader);

            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);

            var configurationService = configurationServiceFactory.Create<FundingConfiguration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var calendarServiceFactory = Provider.GetService<ICalendarServiceFactory>();
            var calendar = calendarServiceFactory.Create(reader);

            var loanServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            var loanService = loanServiceFactory.Create(reader);

            var accrualFactory = Provider.GetService<IAccrualBalanceClientFactory>();
            var accrualService = accrualFactory.Create(reader);

            var queueServiceFactory = Provider.GetService<IAchServiceFactory>();
            var queueService = queueServiceFactory.Create(reader);

            var achConfigServiceFactory = Provider.GetService<IAchConfigurationServiceFactory>();
            var achConfigService = achConfigServiceFactory.Create(reader);
            
            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusManagementService = statusManagementServiceFactory.Create(reader);

            var productService = Provider.GetService<IProductServiceFactory>();
            var product = productService.Create(reader);

            var loanFilterServiceFactory = Provider.GetService<ILoanFilterClientServiceFactory>();
            var loanFilterService = loanFilterServiceFactory.Create(reader);

            var flagServiceFactory= Provider.GetService<IFlagClientFactory>();
            var flagService=flagServiceFactory.Create(reader);
            
            return new PaymentProcessorService(fundingRepository, numberGeneratorService,logger, lookUpService, eventHubService, fundingRequestAttemptsRepository, tenantTimeService , configuration, loanService, accrualService,calendar, queueService, handler,reader, statusManagementService, achConfigService, product,loanFilterService,flagService);
        }
    }
}

