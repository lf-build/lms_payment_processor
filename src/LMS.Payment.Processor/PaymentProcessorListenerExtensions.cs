﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LMS.Payment.Processor
{
    public static class PaymentProcessorListenerExtensions
    {
        public static void UsePaymentProcessorListener(this IApplicationBuilder payment)
        {
            payment.ApplicationServices.GetRequiredService<IPaymentProcessorListener>().Start();
        }
    }
}
