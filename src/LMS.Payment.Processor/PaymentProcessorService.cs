﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.StatusManagement;
using LendFoundry.Calendar.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.NumberGenerator;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.Payment.Processor;
using LMS.Payment.Processor.Persistence;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using LendFoundry.Security.Tokens;
using LMS.Foundation.Amortization;
using LMS.LoanManagement.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.ProductConfiguration;
using LendFoundry.ProductConfiguration.Client;
using Renci.SshNet;
using System.IO;
using System.Text;
using LMS.Loan.Filters.Abstractions;
using LendFoundry.Flag;

namespace LMS.Payment.Processor
{
    public partial class PaymentProcessorService : IPaymentProcessorService
    {
        #region Constructor

        public PaymentProcessorService
            (
                Persistence.IPaymentRepository fundingRepository,
                IGeneratorService fundingNumberGenerator,
                ILogger logger,
                ILookupService lookupService,
                IEventHubClient eventHubClient,
                IPaymentAttemptsRepository fundingRequestAttemptsRepository,
                ITenantTime tenantTime,
                FundingConfiguration fundingConfiguration,
                ILoanManagementClientService loanOnboardingService,
                IAccrualBalanceService accrualService,
                ICalendarService calendarService,
                IAchService achService,
                ITokenHandler tokenParser,
                ITokenReader tokenReader,
                IEntityStatusService statusManagementService,
                IAchConfigurationService achConfigService,
                IProductService productService,
                ILoanFilterService loanFilterService,
                IFlagService flagService
            )
        {
            FundingRepository = fundingRepository;
            FundingNumberGenerator = fundingNumberGenerator;
            FundingRequestAttemptsRepository = fundingRequestAttemptsRepository;
            EventHubClient = eventHubClient;
            CommandExecutor = new CommandExecutor(logger);
            Logger = logger;
            TenantTime = tenantTime;
            LookupService = lookupService;
            FundingConfiguration = fundingConfiguration;
            CalendarService = calendarService;
            LoanOnboardingService = loanOnboardingService;
            AccrualService = accrualService;
            AchService = achService;
            StatusManagementService = statusManagementService;
            TokenParser = tokenParser;
            TokenReader = tokenReader;
            AchConfigService = achConfigService;
            ProductService = productService;
            LoanFilterService = loanFilterService;
            FlagService = flagService;
        }

        #endregion Constructor

        #region Private Properties
        private IAccrualBalanceService AccrualService { get; }
        private CommandExecutor CommandExecutor { get; }
        private IEventHubClient EventHubClient { get; }
        private FundingConfiguration FundingConfiguration { get; }
        private IGeneratorService FundingNumberGenerator { get; }
        private Persistence.IPaymentRepository FundingRepository { get; }
        private IPaymentAttemptsRepository FundingRequestAttemptsRepository { get; }
        private ILookupService LookupService { get; }
        private ITenantTime TenantTime { get; }
        private ICalendarService CalendarService { get; }
        private ILoanManagementClientService LoanOnboardingService { get;  }
        private IAchService AchService { get; }
        private ILogger Logger { get; }
        private IEntityStatusService StatusManagementService { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }
        private IAchConfigurationService AchConfigService { get; }
        private IProductService ProductService { get; }
        private ILoanFilterService LoanFilterService { get; }
        private IFlagService FlagService { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IFunding> AddFundingRequest(string entityType, string entityId, IFundingRequest request)
        {
            entityType = EnsureEntityType(entityType);
            
            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (request.PaymentInstrumentType != PaymentInstrumentType.Cash && request.PaymentInstrumentType != PaymentInstrumentType.TDS)
            {
                var validateBankRTNIFSCCodeRegex = "^[0-9]*$";
                if (!string.IsNullOrEmpty(FundingConfiguration.BankRTNIFSCValidation))
                {
                    validateBankRTNIFSCCodeRegex = FundingConfiguration.BankRTNIFSCValidation;
                }

                if (string.IsNullOrWhiteSpace(request.BankAccountNumber))
                {
                    throw new ArgumentNullException(request.BankAccountNumber);
                }

                if (string.IsNullOrWhiteSpace(request.BankRTN))
                {
                    throw new ArgumentNullException(request.BankRTN);
                }

                if (string.IsNullOrWhiteSpace(request.BorrowersName))
                {
                    throw new ArgumentNullException(request.BorrowersName);
                }

                if (!Regex.IsMatch(request.BankAccountNumber, "^[a-zA-Z0-9]+$"))
                {
                    throw new InvalidArgumentException(request.BankAccountNumber);
                }

                if (!Regex.IsMatch(request.BankRTN, validateBankRTNIFSCCodeRegex))
                {
                    throw new InvalidArgumentException(request.BankRTN);
                }

                if (!Regex.IsMatch(request.BorrowersName, "^([a-zA-Z\\s]+)$"))
                {
                    throw new ArgumentException(request.BorrowersName);
                }
                
                if (request.PaymentInstrumentType == PaymentInstrumentType.Check)
                {
                    if (string.IsNullOrWhiteSpace(request.CheckNo))
                    {
                        throw new ArgumentNullException(request.CheckNo);
                    }
                }
            }

            if (request.PaymentAmount < 0.01)
            {

                request.PaymentAmount = request.InterestAmount + request.AdditionalInterestAmount +
                                        request.PrincipalAmount + request.Fees.Sum(x => x.FeeAmount);
                
                if (request.PaymentAmount < 0.01)
                {
                    throw new InvalidArgumentException("Amount not given");
                }
            }

            IFunding applicationFunding = new Funding(request);
                var instructionDate = GetPreviousBusinessDay(request.PaymentScheduleDate);

            await Task.Run(() =>
            {
                var AddFundingRequestCommand = new Command(
                    () =>
                    {
                        applicationFunding.EntityId = entityId;
                        applicationFunding.EntityType = entityType;
                        applicationFunding.LoanNumber = entityId;
                        applicationFunding.InitiatedBy = EnsureCurrentUser();
                        applicationFunding.FundingCreatedDate = new TimeBucket(TenantTime.Now);
                        applicationFunding.ReferenceId = FundingNumberGenerator.TakeNext("fedchex").Result;
                        applicationFunding.RequestStatus = string.IsNullOrWhiteSpace(request.Status) ?
                            LookupService.GetLookupEntry("requeststatus", "initiated").FirstOrDefault().Key :
                            LookupService.GetLookupEntry("requeststatus", request.Status).FirstOrDefault().Key;
                        applicationFunding.InstructionCreationDate = new TimeBucket(instructionDate);
                        applicationFunding.RetryCount = 0;
                        FundingRepository.Add(applicationFunding);
                    },
                    () => FundingRepository.Remove(applicationFunding)
                );
                CommandExecutor.Execute(new List<Command> { AddFundingRequestCommand });
            });
            await EventHubClient.Publish(new PaymentRequestAdded
            {
                EntityId = applicationFunding.EntityId,
                EntityType = applicationFunding.EntityType,
                ApplicationFunding = applicationFunding
            });
            return applicationFunding;
        }


        public async Task<IFunding> MakeRefundPayment(string entityType, string entityId, IFundingRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            entityType = EnsureEntityType(entityType);

            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (request.PaymentAmount < 0.01)
            {
                throw new InvalidArgumentException("Invalid payment amount");
            }

            var refundData = await GetFundingData(entityType, entityId);
            if (refundData != null && refundData.Count > 0)
            {
                refundData = refundData.Where(x => x.RequestStatus == "initiated" && x.PaymentType == PaymentType.Refund).ToList();
                var initiatedRefundAmount = refundData != null && refundData.Count > 0 ? refundData.Sum(x => x.PaymentAmount) : 0;

                var activeSchedule = await LoanOnboardingService.GetLoanSchedule(entityId);
                if (activeSchedule == null)
                {
                    throw new NotFoundException($"There is no active schedule for LoanNumber {entityId}");
                }

                var accrualDetails = await AccrualService.GetLoanByScheduleVersion(entityId, activeSchedule.ScheduleVersionNo);
                if (accrualDetails == null)
                {
                    throw new NotFoundException($"Accrual detail not found for LoanNumber {entityId}");
                }

                if (initiatedRefundAmount + request.PaymentAmount >
                    (accrualDetails.ExcessMoney != null ? accrualDetails.ExcessMoney.Amount : 0))
                {
                    throw new InvalidArgumentException(
                        "Invalid Refund amount - Initiated refund amount should be less than excess amount");
                }

            }
            request.PaymentType = PaymentType.Refund;
            request.TransactionType = TransactionType.Debit;
            request.Status = (request.PaymentInstrumentType != PaymentInstrumentType.Ach) 
                                ? LookupService.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key 
                                : LookupService.GetLookupEntry("requeststatus", "initiated").FirstOrDefault().Key;
            request.RequestedDate = TenantTime.Now;

            await AccrualService.UpdateExcessPayment(entityId, request.PaymentAmount, false);

            var fundingData = await AddFundingDetails(entityType, entityId, request);
            return fundingData;
        }
        
        public async Task<ILoanPaymentAttempts> AddFundingRequestAttempts(string entityType, string entityId,
            IPaymentRequestAttempts request)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            
            ILoanPaymentAttempts applicationFundingAttempt = new LoanFundingRequestAttempts(request);

            await Task.Run(() =>
            {
                var AddFundingRequestCommand = new Command(
                    () =>
                    {
                        applicationFundingAttempt.EntityId = entityId;
                        applicationFundingAttempt.EntityType = entityType;
                        applicationFundingAttempt.LoanNumber = entityId;
                        
                        // TODO what if there are > 1 attempts in the same second? Does this have to be unique?
                        applicationFundingAttempt.FundingRequestAttemptId = entityId + "_" + TenantTime.Now.DateTime.ToString("yyyyMMddHHmmss");
                        applicationFundingAttempt.ProviderReferenceNumber = request.ProviderReferenceNumber;
                        applicationFundingAttempt.ReturnCode = request.ReturnCode;
                        applicationFundingAttempt.ReturnDate = new TimeBucket(request.ReturnDate);
                        applicationFundingAttempt.ProviderResponseDate = new TimeBucket(request.ProviderResponseDate);
                        applicationFundingAttempt.ProviderResponseStatus = request.ProviderResponseStatus;
                        applicationFundingAttempt.ReferenceId = request.ReferenceId;
                        FundingRequestAttemptsRepository.Add(applicationFundingAttempt);
                    },
                    () => FundingRequestAttemptsRepository.Remove(applicationFundingAttempt)
                );
                CommandExecutor.Execute(new List<Command> { AddFundingRequestCommand });
            });
            return applicationFundingAttempt;
        }
        
        public async Task<IEnumerable<IFunding>> GetFundingRequest(List<string> statuses)
        {
            if (statuses == null || !statuses.Any())
            {
                throw new ArgumentException($"{nameof(statuses)} is mandatory");
            }

            return await FundingRepository.GetFundingRequest(statuses);
        }

        public async Task<List<IFunding>> GetFundingData(string entityType, string entityId)
        {
            if (string.IsNullOrEmpty(entityType))
            {
                throw new ArgumentException($"{nameof(entityType)} is mandatory");
            }

            if (string.IsNullOrEmpty(entityId))
            {
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            }

            var fundingData = await FundingRepository.GetFundingRequest(entityType, entityId);
            if (fundingData == null)
            {
                throw new NotFoundException($"No funding data found for {entityType}-{entityId}");
            }

            return fundingData;

        }
        
        public async Task<List<IFunding>> GetInProgressPayments(string entityType, string entityId)
        {
            if (string.IsNullOrEmpty(entityType))
            {
                throw new ArgumentException($"{nameof(entityType)} is mandatory");
            }

            if (string.IsNullOrEmpty(entityId))
            {
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            }

            var fundingData = await FundingRepository.GetFundingRequest(entityType, entityId);
            if (fundingData == null)
            {
                throw new NotFoundException($"No funding data found for {entityType}-{entityId}");
            }

            return fundingData.Where(x => x.RequestStatus == "initiated" || x.RequestStatus == "submitted").ToList();
        }

        public async Task<IFunding> GetFundingByReferenceId(string referenceId)
        {
            if (string.IsNullOrEmpty(referenceId))
            {
                throw new ArgumentException($"{nameof(referenceId)} is mandatory");
            }

            var fundingData = await FundingRepository.GetFundingByReferenceId(referenceId);

            return fundingData;
        }

        public async Task<IFunding> UpdateFundingStatus(string entityType, string entityId, IUpdateFundingStatus updaterequest)
        {
            if (updaterequest.status == null || !updaterequest.status.Any())
            {
                throw new ArgumentException($"{nameof(updaterequest.status)} is mandatory");
            }

            if (entityId == null || !entityId.Any())
            {
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            }

            if (string.IsNullOrWhiteSpace(updaterequest.ReferenceId))
            {
                throw new ArgumentException($"{nameof(updaterequest.ReferenceId)} is mandatory");
            }

            var result = await FundingRepository.UpdateFundingStatus(entityType, entityId, TenantTime.Now, updaterequest);

            await EventHubClient.Publish(new LoanStatusUpdated
            {
                EntityType = entityType,
                EntityId = result.EntityId,
                Response = result,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });
            return result;
        }

        public async Task<IFunding> UpdateRetryCount(string entityType, string entityId, IUpdateRetryCount updateRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            }

            if (string.IsNullOrWhiteSpace(updateRequest.ReferenceId))
            {
                throw new ArgumentException($"{nameof(updateRequest.ReferenceId)} is mandatory");
            }

            // TODO how do we know this isn't inadvertently being down multiple times? 
            // TODO This needs to be handled with current retry count passed - updated only if it matches
            // TODO or some other similar mechanism
            var result = await FundingRepository.UpdateRetryCount(entityType, entityId, updateRequest);

            return result;
        }

        public void UpdateFundingData(IFunding fundingData)
        {
            if (fundingData == null)
            {
                throw new ArgumentException($"{nameof(fundingData)} is mandatory");
            }

            FundingRepository.UpdateFunding(fundingData);
        }

        public async Task<IFunding> UpdateNextRetryDate(string entityType, string entityId, IUpdateRetryCount updateRequest)
        {
            if (string.IsNullOrWhiteSpace(updateRequest.ReferenceId))
                throw new ArgumentException($"{nameof(updateRequest.ReferenceId)} is mandatory");
            var result = await FundingRepository.UpdateNextRetryDate(entityType, entityId, updateRequest.ReferenceId, updateRequest.nextRetryDate);
            return result;
        }

        public async Task<ILoanPaymentAttempts> UpdateAttamptStatus(string referenceId,
            IUpdateFundingAttemptStatusRequest updateRequest)
        {
            return await UpdateAttemptStatus(referenceId, updateRequest);
        }

        public async Task<ILoanPaymentAttempts> UpdateAttemptStatus(string referenceId, IUpdateFundingAttemptStatusRequest updateRequest)
        {
            if (string.IsNullOrWhiteSpace(referenceId))
            {
                throw new ArgumentException($"{nameof(referenceId)} is mandatory");
            }

            // TODO how do we know that the status is already not updated OR if it is overwriting another update?
            // TODO a mechanism of version GET followed by POST the value gotten must be implemented
            // TODO otherwise we can get skips / overwrites in updates
            return await FundingRequestAttemptsRepository.UpdateStatus(referenceId, updateRequest);
        }

        public async Task<IFunding> ReSubmitPayment(string entityType, string entityId, IReSubmitPaymentRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
            {
                throw new ArgumentNullException(nameof(entityId));
            }

            EnsureEntityType(entityType);

            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (string.IsNullOrWhiteSpace(request.PaymentRequestId))
            {
                throw new ArgumentException($"{nameof(request.PaymentRequestId)} is mandatory");
            }

            if (string.IsNullOrWhiteSpace(request.PaymentInstrumentId))
            {
                throw new ArgumentException($"{nameof(request.PaymentInstrumentId)} is mandatory");
            }

            var applicationFunding = await FundingRepository.Get(request.PaymentRequestId);
            if (applicationFunding == null)
            {
                throw new NotFoundException($"No funding data found for {request.PaymentRequestId}");
            }

            var loanDetails = await LoanOnboardingService.GetLoanInformationByLoanNumber(entityId);
            var instrument = loanDetails.PaymentInstruments.FirstOrDefault(x => x.Id == request.PaymentInstrumentId);
            if (instrument == null)
            {
                throw new NotFoundException(
                    $"Payment InstrumentId {request.PaymentInstrumentId} is not found in loan details");
            }

            var bankDetails = await LoanOnboardingService.GetAllBankDetails(entityId);
            var bank = bankDetails.FirstOrDefault(x => x.PaymentInstrumentId == request.PaymentInstrumentId);
            if (bank == null)
            {
                throw new NotFoundException($"Bank Details  not found for instrumentId {request.PaymentInstrumentId}");
            }

            await Task.Run(() =>
            {
                var AddFundingRequestCommand = new Command(
                    () =>
                    {
                        applicationFunding.PaymentInstrumentType = instrument.PaymentInstrumentType;
                        applicationFunding.BankAccountNumber = bank.AccountNumber;
                        applicationFunding.BankRTN = bank.RoutingNumber;
                        applicationFunding.BorrowersName = bank.BankHolderName;
                        applicationFunding.BankAccountType = bank.AccountType.ToString();
                        applicationFunding.RequestStatus = LookupService.GetLookupEntry("requeststatus", "resubmitted").FirstOrDefault().Key;
                        applicationFunding.IsAccounted = false;
                        applicationFunding.IsFileCreated = false;
                        applicationFunding.IsReSubmitted = true;
                        applicationFunding.ResubmitType = ResubmitType.Manual;
                        FundingRepository.UpdateFunding(applicationFunding);
                    },
                    // TODO convey to caller via exception, return value
                    () => FundingRepository.Remove(applicationFunding)
                );
                CommandExecutor.Execute(new List<Command> { AddFundingRequestCommand });
            });

            await EventHubClient.Publish(new LoanStatusUpdated
            {
                EntityType = applicationFunding.EntityType,
                EntityId = applicationFunding.EntityId,
                Response = applicationFunding,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });

            return applicationFunding;
        }

        public async Task<List<ILoanPaymentAttempts>> GetAttemptData(string referenceId)
        {

            if (string.IsNullOrEmpty(referenceId))
            {
                throw new ArgumentException($"{nameof(referenceId)} is mandatory");
            }

            var fundingAttemptData = await FundingRequestAttemptsRepository.GetAttemptData(referenceId);

            return fundingAttemptData;
        }

        public async Task UpdateFundingSuccess(object objData)
        {
            var FileId = GetFileId(objData);
            var file = AchService.GetById(FileId);
            var instructionReferenceNumbers = file.Batches.SelectMany(batch => batch.Data).Select(batchData => batchData.InstructionInternalReferenceNumber).ToList();
            var instructions = AchService.GetByInternalReferenceNumber(new InstructionDetailRequest() { InternalReferenceNumbers = instructionReferenceNumbers }).ToList();

            foreach (var ins in instructions)
            {
                var fundingRequest = await FundingRequestAttemptsRepository.GetAttemptData(ins.ReferenceNumber, ins.InternalReferenceNumber.ToString());
                if (fundingRequest != null)
                {
                    var paymentData = await GetFundingByReferenceId(ins.ReferenceNumber);
                    if (paymentData.PaymentType == PaymentType.Funding || paymentData.PaymentType == PaymentType.Refund)
                    {

                        fundingRequest.ProviderResponseDate = new TimeBucket(TenantTime.Now);
                        fundingRequest.ProviderResponseStatus = LookupService.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key;
                        fundingRequest.IsFileCreated = true;
                        fundingRequest.IsAccounted = false;
                        fundingRequest.FileCreatedDate = new TimeBucket(TenantTime.Now);
                        await Task.Run(() => FundingRequestAttemptsRepository.Update(fundingRequest));
                        paymentData.IsFileCreated = true;
                        paymentData.FileCreatedDate = new TimeBucket(TenantTime.Now);
                        paymentData.RequestStatus = LookupService.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key;
                        await Task.Run(() => FundingRepository.Update(paymentData));

                        await EventHubClient.Publish("FundingStatusSuccess", new
                        {
                            EntityId = ins.ReferenceNumber,
                            EntityType = "loan",
                            Response = paymentData
                        });
                    }
                    else
                    {
                        fundingRequest.IsFileCreated = true;
                        fundingRequest.IsAccounted = false;
                        fundingRequest.FileCreatedDate = new TimeBucket(file.ProcessEndDateTime);
                        await Task.Run(() => FundingRequestAttemptsRepository.Update(fundingRequest));

                        paymentData.IsFileCreated = true;
                        paymentData.IsAccounted = false;
                        paymentData.FileCreatedDate = new TimeBucket(file.ProcessEndDateTime); //TODO: to pass ProcessEndDateTime
                        await Task.Run(() => FundingRepository.Update(paymentData));
                    }
                }
            }
        }
        
        public async Task UpdateFailFunding(object objData)
        {
            List<PaymentRejectedDetail> failures = new List<PaymentRejectedDetail>();
            var instruction = GetInstruction(objData);
            var reason = GetReasonValue(objData);
            try
            {    
                if (string.IsNullOrEmpty(instruction.ReferenceNumber))
                    throw new ArgumentException($"{nameof(instruction.ReferenceNumber)} is mandatory");
                if (instruction.InternalReferenceNumber == 0)
                    throw new ArgumentException($"{nameof(instruction.InternalReferenceNumber)} is mandatory");

                var fundingRequest = await FundingRequestAttemptsRepository.GetAttemptData(instruction.ReferenceNumber, instruction.InternalReferenceNumber.ToString());
                if(fundingRequest == null)
                {
                    throw new ArgumentException($"No funding data found for ReferenceNumber: {instruction.ReferenceNumber} and InternalReferenceNumber: {instruction.InternalReferenceNumber}");
                }
                fundingRequest.ReturnCode = instruction.ReturnCode;
                fundingRequest.ReturnReason = reason;
                fundingRequest.ReturnDate = new TimeBucket(TenantTime.Now);
                fundingRequest.ProviderResponseDate = new TimeBucket(TenantTime.Now);
                fundingRequest.ProviderResponseStatus = instruction.Status.ToString();
                fundingRequest.AttemptStatus = "failed";
                await Task.Run(() => FundingRequestAttemptsRepository.Update(fundingRequest));

                var fundingData = await UpdateFundingStatus("loan", fundingRequest.LoanNumber, new UpdateFundingStatus { ReferenceId = fundingRequest.ReferenceId, status = "failed" });
                if ((fundingData.PaymentType != PaymentType.Refund) || (fundingData.PaymentType != PaymentType.Funding))
                {
                    var payments = await AccrualService.GetTransactions(fundingRequest.LoanNumber);
                    ITransaction lastPayment = null;
                    try
                    {
                        var loanInfo = await LoanOnboardingService.GetLoanSchedule(fundingRequest.LoanNumber);
                        if (loanInfo.PaymentFrequency == PaymentFrequency.Daily)
                        {
                            lastPayment = payments.Where(x => (x.TransactionPurpose == fundingData.PaymentPurpose || x.TransactionPurpose == "payoff") && x.Amount == fundingData.PaymentAmount && x.IsReverse == false && x.TransactionDate == fundingData.PaymentScheduleDate.Time.Date).OrderByDescending(x => x.TransactionDateTime).FirstOrDefault();
                        }
                        else
                        {
                            lastPayment = payments.Where(x => (x.TransactionPurpose == fundingData.PaymentPurpose || x.TransactionPurpose == "payoff") && x.Amount == fundingData.PaymentAmount && x.IsReverse == false).OrderByDescending(x => x.TransactionDateTime).FirstOrDefault();
                        }
                        var reversePayment = await AccrualService.ReversePayment(fundingRequest.LoanNumber, new ReversePayment() { TransactionReferenceId = lastPayment.Id, ProcessingDate = TenantTime.Now.UtcDateTime });
                        //TODO:need to pass tenanttime.now as processingDate
                    }
                    catch (Exception e)
                    {
                        Logger.Error("ReversePayment Error From Accounting LoanNumber " + instruction.ReferenceNumber + "Error " + e.Message, e);
                        failures.Add(new PaymentRejectedDetail(instruction, reason, e.Message));
                    }
                    var attempts = await GetAllAttemptData(fundingRequest.LoanNumber);
                    var failureAttempts = 0;
                    var nsfFailureCount = 0;
                    var successiveFailureAttempts = 0;
                    var successiveNsfFailureCount = 0;
                    if (attempts != null && attempts.Count > 0)
                    {
                        failureAttempts = attempts.OrderByDescending(c => c.AttemptDate.Time.DateTime).Where(x => x.AttemptStatus == "failed").ToList().Count;
                        successiveFailureAttempts = failureAttempts;
                        var lastSuccessAttempt = attempts.FirstOrDefault(x => x.AttemptStatus == "paymentsuccess");
                        if (lastSuccessAttempt != null)
                        {
                            successiveFailureAttempts = attempts.Where(x => x.AttemptDate.Time.DateTime > lastSuccessAttempt.AttemptDate.Time.DateTime && x.AttemptStatus == "failed").ToList().Count;
                        }
                    }
                    await EventHubClient.Publish("FailedPayment", new
                    {
                        EntityId = fundingRequest.LoanNumber,
                        EntityType = "loan",
                        Response = fundingData,
                        LoanNumber = fundingRequest.LoanNumber,
                        FailCount = failureAttempts.ToString(),
                        SuccessiveFailCount = successiveFailureAttempts.ToString()
                    });

                    if (instruction.ReturnCode == "R01")
                    {
                        var nsfFailureList = attempts.Where(x => x.AttemptStatus == "failed" && x.ReturnCode == "R01").ToList();
                        nsfFailureCount = nsfFailureList.Count;
                        successiveNsfFailureCount = nsfFailureCount;
                        var lastSuccessAttempt = attempts.FirstOrDefault(x => x.AttemptStatus == "paymentsuccess");
                        if (lastSuccessAttempt != null)
                        {
                            successiveNsfFailureCount = attempts.Where(x => x.AttemptDate.Time > lastSuccessAttempt.AttemptDate.Time && x.AttemptStatus == "failed" && x.ReturnCode == "R01").ToList().Count;
                        }
                        var nextRetryDate = GetNextRetryDay(TenantTime.Now.Date, FundingConfiguration.NextRetryDays);
                        await UpdateNextRetryDate("loan", fundingRequest.LoanNumber, new UpdateRetryCount { ReferenceId = fundingRequest.ReferenceId, nextRetryDate = nextRetryDate });

                        await EventHubClient.Publish("NSFFailure", new
                        {
                            EntityId = fundingRequest.LoanNumber,
                            EntityType = "loan",
                            Response = fundingData,
                            LoanNumber = fundingRequest.LoanNumber,
                            FailCount = nsfFailureCount.ToString(),
                            SuccessiveFailCount = successiveNsfFailureCount.ToString()
                        });
                    }
                }
                else if (fundingData.PaymentType == PaymentType.Refund)
                {
                    await AccrualService.UpdateExcessPayment(fundingRequest.LoanNumber, fundingData.PaymentAmount, true);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error while updating failed payment", ex);
                failures.Add(new PaymentRejectedDetail(instruction, reason, ex.Message));
            }
            finally
            {
                if(FundingConfiguration.FailedCSVCreate)
                {
                    if(failures.Any())
                    {
                        var csvContent = CreateFailedProcessingFile(failures, FundingConfiguration.CSVColumnFormat);
                        if(csvContent != null)
                        {
                            var csvByteArray = Encoding.ASCII.GetBytes(csvContent.ToString());
                            var csvMemoryStream = new MemoryStream(csvByteArray);
                            UploadFile(csvMemoryStream);
                        }
                    }
                }
            }
        }

        public async Task UpdateNOA(object objData)
        {
            var instruction = GetInstruction(objData);
            var reason = GetNOAReasonValue(objData);
            if (string.IsNullOrEmpty(instruction.ReferenceNumber))
                throw new ArgumentException($"{nameof(instruction.ReferenceNumber)} is mandatory");
            if (instruction.InternalReferenceNumber == 0)
                throw new ArgumentException($"{nameof(instruction.InternalReferenceNumber)} is mandatory");

            var fundingRequest = await FundingRequestAttemptsRepository.GetAttemptData(instruction.ReferenceNumber, instruction.InternalReferenceNumber.ToString());

            fundingRequest.ReturnReason = reason;
            fundingRequest.ReturnCode = instruction.ReturnCode;
            fundingRequest.ReturnDate = new TimeBucket(instruction.ExecutionDate); // pass current datetime
            fundingRequest.ProviderResponseDate = new TimeBucket(TenantTime.Now);
            fundingRequest.ProviderResponseStatus = instruction.Status.ToString();
            await Task.Run(() => FundingRequestAttemptsRepository.Update(fundingRequest));
            var fundingData = await UpdateFundingStatus("loan", fundingRequest.LoanNumber, new UpdateFundingStatus { ReferenceId = fundingRequest.ReferenceId, status = "noa" });
            await EventHubClient.Publish("NOAPayment", new
            {
                EntityId = fundingRequest.LoanNumber,
                EntityType = "loan",
                Response = fundingData
            });
        }
        
        public async Task<List<IFunding>> GetFundingRequestsByDate(SearchRequest requestdata)
        {
            var fundingData = await FundingRepository.GetFundingRequestsByDate(requestdata.StartDate, requestdata.EndDate);
            return fundingData;
        }
        
        public async Task<List<IFunding>> GetFundingAccountedByDate(SearchRequest requestdata)
        {
            var fundingData = await FundingRepository.GetFundingAccountedByDate(requestdata.StartDate, requestdata.EndDate);
            return fundingData;
        }

        public async Task<List<ILoanPaymentAttempts>> GetPaymentAttempts()
        {
            var fundingData = await FundingRequestAttemptsRepository.GetPaymentAttempts();
            return fundingData;
        }

        public async Task<List<IFunding>> GetFundingAttemptsByReferenceIds(PaymentDetailsRequest paymentDetailsRequest)
        {
            if (paymentDetailsRequest == null)
                throw new ArgumentException("Argument is null ", nameof(paymentDetailsRequest));

            var fundingData = await FundingRepository.GetFundingAttemptsByReferenceIds(paymentDetailsRequest.ReferenceNumbers);
            return fundingData;
        }

        public async Task<List<AchDetails>> GetAttampts(AttemptRequest attemptRequest)
        {
            return await GetAttampts(attemptRequest);
        }

        public async Task<List<AchDetails>> GetAttempts(AttemptRequest attemptRequest)
        {
            if (attemptRequest == null)
                throw new ArgumentException("Argument is null ", nameof(attemptRequest));
            if (attemptRequest.ToDate < attemptRequest.FromDate)
                throw new ArgumentException("To date should be less then from date");
            attemptRequest.ToDate = attemptRequest.ToDate.AddDays(1);

            List<ILoanPaymentAttempts> fundingData = null;
            if (attemptRequest.ReportType == ReportType.InstructionCreated)
            {
                fundingData = await FundingRequestAttemptsRepository.GetAttempts(attemptRequest);
            }
            else
            {
                fundingData = await FundingRequestAttemptsRepository.GetReSubmitAttempts(attemptRequest);
            }
            var funders = AchConfigService.GetFundingSources();
            var products = await ProductService.GetAll();
            List<AchDetails> achReportData = new List<AchDetails>();
            foreach (var attempts in fundingData)
            {
                try
                {

                    var payment = await FundingRepository.GetFundingByReferenceId(attempts.ReferenceId);
                    var loanSchedule = await LoanOnboardingService.GetLoanSchedule(attempts.LoanNumber);
                    var loan = await LoanOnboardingService.GetLoanInformationByLoanNumber(attempts.LoanNumber);
                    var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow("loan", attempts.LoanNumber);
                    var accrualInfo = await AccrualService.GetLoanByScheduleVersion(attempts.LoanNumber, loanSchedule.ScheduleVersionNo);

                    var achRecord = new AchDetails();

                    achRecord.LoanNumber = attempts.LoanNumber;
                    achRecord.AttemptDate = attempts.AttemptDate;
                    achRecord.AchReferenceId = attempts.ReferenceId;
                    achRecord.InstructionStatus = attempts.AttemptStatus;
                    achRecord.PaymentScheduleDate = payment != null ? payment.PaymentScheduleDate : null;
                    achRecord.BorrowerName = payment != null ? payment.BorrowersName : "";
                    var schedule = loanSchedule.ScheduleDetails.FirstOrDefault(C =>
                            C.ScheduleDate.Date == payment.PaymentScheduleDate.Time.Date);
                    IBankDetails bank = null;
                    if (attemptRequest.ReportType == ReportType.InstructionCreated)
                    {
                        var bankData = await LoanOnboardingService.GetAllBankDetails(attempts.LoanNumber);
                        bank = bankData.FirstOrDefault(x => x.RoutingNumber == payment.BankRTN);
                        achRecord.BankName = bank != null ? bank.Name : string.Empty;
                        achRecord.RoutingNumber = payment.BankRTN;
                        achRecord.PaymentNumber = schedule != null ? schedule.Installmentnumber : 0;
                    }
                    else if (attemptRequest.ReportType == ReportType.ReSubmit)
                    {
                        var attemptData = await FundingRequestAttemptsRepository.GetAttemptData(payment.ReferenceId);

                        achRecord.ReAttemptType = payment.ResubmitType.ToString();
                        achRecord.ReturnCode = attempts.ReturnCode;
                        achRecord.InstallmentAmount = schedule != null ? schedule.PaymentAmount : 0;
                        achRecord.ReturnCodeDescription = attempts.ReturnReason;
                        achRecord.AchPauseStatus = loan.IsAutoPay ? "Yes" : "No";
                        achRecord.AchPauseMode = loan != null ? loan.AutoPayType.ToString() : string.Empty;
                        achRecord.AccrualPauseMode = accrualInfo != null ? accrualInfo.AccrualMode.ToString() : string.Empty;
                        achRecord.AccrualPauseStatus = accrualInfo.AccrualStop ? "Yes" : "No";
                        achRecord.LastPaymentAttemptDate = attemptData != null && attemptData.Count > 0 ? attemptData.LastOrDefault().AttemptDate : null;
                    }

                    achRecord.AttemptNumber = attempts.AttemptCount;
                    achRecord.AchAmount = payment.PaymentAmount;
                    achRecord.BorrowerEmail = loan.PrimaryApplicantDetails.Emails.FirstOrDefault()?.EmailAddress;
                    achRecord.BorrowerPhone = loan.PrimaryApplicantDetails.Phones.FirstOrDefault()?.PhoneNo;
                    achRecord.PrincipalOutstandingAmount = accrualInfo != null && accrualInfo.PBOT != null ? accrualInfo.PBOT.TotalPrincipalOutStanding : 0;
                    achRecord.InterestOutstandingAmount = accrualInfo != null && accrualInfo.PBOT != null ? accrualInfo.PBOT.TotalInterestOutstanding : 0;
                    achRecord.FeeOutstandingAmount = accrualInfo != null && accrualInfo.PBOT != null ? accrualInfo.PBOT.TotalFeeOutstanding : 0;
                    achRecord.TotalOutstandingAmount = accrualInfo != null && accrualInfo.PBOT != null ? accrualInfo.PBOT.TotalPrincipalOutStanding + accrualInfo.PBOT.TotalInterestOutstanding + accrualInfo.PBOT.TotalFeeOutstanding : 0;
                    achRecord.CurrentOutstandingAmount = accrualInfo != null && accrualInfo.PBOC != null ? accrualInfo.PBOC.CurrentInterestOutStanding + accrualInfo.PBOC.CurrentPrincipalOutStanding : 0;
                    achRecord.LoanProductId = loan.LoanProductId;
                    achRecord.StatusWorkFlowId = statusWorkFlow != null ? statusWorkFlow.StatusWorkFlowId : "";
                    if(loan != null && !string.IsNullOrEmpty(loan.FunderId) && funders != null && funders.Count > 0){
                        achRecord.FunderId = loan.FunderId;
                        var filteredFunder =  funders.FirstOrDefault(x => x.Id == loan.FunderId);
                        if(filteredFunder != null){
                            achRecord.FunderName = filteredFunder.Name;
                        }
                    }
                    if(loan != null && !string.IsNullOrEmpty(loan.LoanProductId) && products != null && products.Count > 0){
                        var filteredProduct =  products.FirstOrDefault(x => x.ProductId == achRecord.LoanProductId);
                        if(filteredProduct != null){
                            achRecord.ProductName = filteredProduct.Name;
                        }
                    }
                    achReportData.Add(achRecord);
                }
                catch (Exception e)
                {
                    Logger.Error($"Error  in GetAttempts for Type {ReportType.InstructionCreated.ToString()} for loan {attempts.LoanNumber} : ", e);

                }

            }

            return achReportData;
        }

        public async Task<List<AchDetails>> GetACHFailureReport(AttemptRequest attemptRequest)
        {
            if (attemptRequest == null)
                throw new ArgumentException("Argument is null ", nameof(attemptRequest));
            if (attemptRequest.ToDate < attemptRequest.FromDate)
                throw new ArgumentException("To date should be less then from date");
            attemptRequest.ToDate = attemptRequest.ToDate.AddDays(1);

            var fundingData = await FundingRequestAttemptsRepository.GetbyReturnDate(attemptRequest);
            if (!string.IsNullOrWhiteSpace(attemptRequest.ReturnCode))
                fundingData = fundingData.Where(x => x.ReturnCode == attemptRequest.ReturnCode).ToList();

            var funders = AchConfigService.GetFundingSources();
            var products = await ProductService.GetAll();
            List<AchDetails> achReportData = new List<AchDetails>();

            foreach (var attempts in fundingData)
            {
                try
                {
                    var payment = await FundingRepository.GetFundingByReferenceId(attempts.ReferenceId);
                    var loanSchedule = await LoanOnboardingService.GetLoanSchedule(attempts.LoanNumber);
                    var loan = await LoanOnboardingService.GetLoanInformationByLoanNumber(attempts.LoanNumber);
                    var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow("loan", attempts.LoanNumber);
                    var accrualInfo = await AccrualService.GetLoanByScheduleVersion(attempts.LoanNumber, loanSchedule.ScheduleVersionNo);

                    var achRecord = new AchDetails();
                    achRecord.LoanNumber = attempts.LoanNumber;
                    achRecord.LoanProductId = loan != null ? loan.LoanProductId : "";
                    achRecord.StatusWorkFlowId = statusWorkFlow != null ? statusWorkFlow.StatusWorkFlowId : "";
                    achRecord.BorrowerName = loan != null ? loan.PrimaryApplicantDetails.Name.First + " " + loan.PrimaryApplicantDetails.Name.Last : "";
                    achRecord.AchAmount = payment != null ? payment.PaymentAmount : 0;
                    achRecord.AttemptNumber = attempts.AttemptCount;
                    var schedule = loanSchedule.ScheduleDetails.FirstOrDefault(C =>
                           C.ScheduleDate.Date == payment.PaymentScheduleDate.Time.Date);
                    achRecord.PaymentNumber = schedule != null ? schedule.Installmentnumber : 0;
                    achRecord.AchReferenceId = attempts.ReferenceId;
                    achRecord.BorrowerName = loan != null ? loan.PrimaryApplicantDetails.Name.First + " " + loan.PrimaryApplicantDetails.Name.Last : "";
                    achRecord.BorrowerEmail = loan.PrimaryApplicantDetails.Emails.FirstOrDefault()?.EmailAddress;
                    achRecord.BorrowerPhone = loan.PrimaryApplicantDetails.Phones.FirstOrDefault()?.PhoneNo;
                    achRecord.InstallmentAmount = schedule != null ? schedule.PaymentAmount : 0;
                    achRecord.TotalOutstandingAmount = accrualInfo != null && accrualInfo.PBOT != null ? accrualInfo.PBOT.TotalPrincipalOutStanding + accrualInfo.PBOT.TotalInterestOutstanding + accrualInfo.PBOT.TotalFeeOutstanding : 0;
                    achRecord.CurrentOutstandingAmount = accrualInfo != null && accrualInfo.PBOC != null ? accrualInfo.PBOC.CurrentInterestOutStanding + accrualInfo.PBOC.CurrentPrincipalOutStanding : 0;
                    achRecord.ReturnCode = attempts.ReturnCode;
                    achRecord.ReturnDate = attempts.ReturnDate;
                    achRecord.AttemptDate = attempts.AttemptDate;
                    achRecord.ReturnCodeDescription = attempts.ReturnReason;
                     if(loan != null && !string.IsNullOrEmpty(loan.FunderId) && funders != null && funders.Count > 0){
                        achRecord.FunderId = loan.FunderId;
                        var filteredFunder =  funders.FirstOrDefault(x => x.Id == loan.FunderId);
                        if(filteredFunder != null){
                            achRecord.FunderName = filteredFunder.Name;
                        }
                    }
                    if(loan != null && !string.IsNullOrEmpty(achRecord.LoanProductId) && products != null && products.Count > 0){
                        var filteredProduct =  products.FirstOrDefault(x => x.ProductId == achRecord.LoanProductId);
                        if(filteredProduct != null){
                            achRecord.ProductName = filteredProduct.Name;
                        }
                    }
                    achReportData.Add(achRecord);
                }
                catch (System.Exception e)
                {
                    Logger.Error($"Error for loan in GetACHFailureReport {attempts.LoanNumber} : ", e);
                }
            }
            return achReportData;
        }
        
        public async Task<List<AchDetails>> GetPaymentReport(AttemptRequest attemptRequest)
        {
            if (attemptRequest == null)
                throw new ArgumentException("Argument is null ", nameof(attemptRequest));
            if (attemptRequest.ToDate < attemptRequest.FromDate)
                throw new ArgumentException("To date should be less then from date");
            attemptRequest.ToDate = attemptRequest.ToDate.AddDays(1);

            var paymentData = await AccrualService.GetPaymentsBetweenEffectiveDate(new PaymentRequest() { ProductId = attemptRequest.ProductId, FromDate = attemptRequest.FromDate, ToDate = attemptRequest.ToDate });
            var funders = AchConfigService.GetFundingSources();
            var products = await ProductService.GetAll();

            List<AchDetails> achReportData = new List<AchDetails>();
            foreach (var attempts in paymentData)
            {
                try
                {
                    var payment = await FundingRepository.GetFundingRequest("loan", attempts.LoanNumber);
                    var loanSchedule = await LoanOnboardingService.GetLoanSchedule(attempts.LoanNumber);
                    var loan = await LoanOnboardingService.GetLoanInformationByLoanNumber(attempts.LoanNumber);
                    var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow("loan", attempts.LoanNumber);
                    var accrualInfo = await AccrualService.GetLoanByScheduleVersion(attempts.LoanNumber, loanSchedule.ScheduleVersionNo);
                    var fees = await AccrualService.GetFees(attempts.LoanNumber);
                    var feeApplied = fees.Where(x => x.AppliedOnDate <= attempts.PaymentDate && x.IsOnBoard == false).Sum(x => x.FeeAmount);
                    var schedulePayment = payment.FirstOrDefault(x => x.PaymentScheduleDate.Time.Date == attempts.PaymentDate.Date);
                    var pbotDetails = await AccrualService.GetPBOTDetails(attempts.LoanNumber, loanSchedule.ScheduleVersionNo);
                    var achRecord = new AchDetails();
                    if(loan != null && !string.IsNullOrEmpty(loan.FunderId) && funders != null && funders.Count > 0){
                        achRecord.FunderId = loan.FunderId;
                        var filteredFunder =  funders.FirstOrDefault(x => x.Id == loan.FunderId);
                        if(filteredFunder != null){
                            achRecord.FunderName = filteredFunder.Name;
                        }
                    }
                    achRecord.LoanNumber = attempts.LoanNumber;
                    achRecord.LoanProductId = loan != null ? loan.LoanProductId : "";
                    if(loan != null && !string.IsNullOrEmpty(achRecord.LoanProductId) && products != null && products.Count > 0){
                        var filteredProduct =  products.FirstOrDefault(x => x.ProductId == achRecord.LoanProductId);
                        if(filteredProduct != null){
                            achRecord.ProductName = filteredProduct.Name;
                        }
                    }
                    achRecord.StatusWorkFlowId = statusWorkFlow != null ? statusWorkFlow.StatusWorkFlowId : "";
                    achRecord.BorrowerName = loan != null ? loan.PrimaryApplicantDetails.Name.First + " " + loan.PrimaryApplicantDetails.Name.Last : "";
                    var principalOutStanding = loanSchedule != null ? loanSchedule.LoanAmount - attempts.CumulativePrincipalPaid : 0;
                    achRecord.AfterPrincipalOutstanding = Math.Round(principalOutStanding, 2);
                    double interstOutstanding = 0;
                    if (loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCA.ToString() || loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCALOC.ToString())
                    {
                        interstOutstanding = loanSchedule != null ? ((loanSchedule.LoanAmount * loanSchedule.FactorRate) - loanSchedule.LoanAmount) - attempts.CumulativeInterestPaid : 0;
                    }
                    else
                    {
                        interstOutstanding = pbotDetails != null && pbotDetails.PBOTAccrualDetails != null && pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date) != null ? pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date).TotalInterestEarned - attempts.CumulativeInterestPaid: 0;
                    }
                    var priorPrincipalOutstanding = pbotDetails != null && pbotDetails.PBOTAccrualDetails != null && pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date) != null ? pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date).PrincipalBalanceOutstanding: 0;

                    var priorInterestOutstanding = pbotDetails != null && pbotDetails.PBOTAccrualDetails != null && pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date) != null ? pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date).TotalInterestEarned : 0;

                    var priorFeeOutstanding = pbotDetails != null && pbotDetails.PBOTAccrualDetails != null && pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date) != null ? pbotDetails.PBOTAccrualDetails.FirstOrDefault(x => x.ProcessingDate == attempts.EffectiveDate.AddDays(-1).Date).TotalFeeOutStanding : 0;


                    achRecord.AfterInterestOutstanding = Math.Round(interstOutstanding, 2);
                    var feeOutstanding = (feeApplied - attempts.CumulativeFeePaid) < 0 ? 0 : (feeApplied - attempts.CumulativeFeePaid);
                    achRecord.AfterFeeOutstanding = Math.Round(feeOutstanding, 2);
                    achRecord.AfterLoanOutstanding = Math.Round(principalOutStanding + interstOutstanding + feeOutstanding, 2);
                    achRecord.CumulativePrincipalReceived = Math.Round(attempts.CumulativePrincipalPaid, 2);
                    achRecord.CumulativeInterestReceived = Math.Round(attempts.CumulativeInterestPaid, 2);
                    achRecord.CumulativeFeeReceived = Math.Round(attempts.CumulativeFeePaid, 2);
                    achRecord.CumulativeAmountReceived = Math.Round(attempts.CumulativePrincipalPaid + attempts.CumulativeInterestPaid + attempts.CumulativeFeePaid + attempts.CumulativePriorInterestPaid, 2);
                    achRecord.PriorPrincipalOutstanding = Math.Round(achRecord.PriorPrincipalOutstanding + principalOutStanding, 2);
                    achRecord.PriorInterestOutstanding = Math.Round(achRecord.PriorInterestOutstanding + priorInterestOutstanding + attempts.PriorInterest, 2);
                    achRecord.PriorFeeOutstanding = Math.Round(achRecord.PriorFeeOutstanding + priorFeeOutstanding, 2);
                    achRecord.PriorLoanOutstanding = Math.Round(achRecord.PriorLoanOutstanding +achRecord.PriorPrincipalOutstanding + achRecord.PriorInterestOutstanding + achRecord.PriorFeeOutstanding, 2);
                    achRecord.PaymentPurpose = schedulePayment != null ? schedulePayment.PaymentPurpose : "";
                    achRecord.PaymentMethod = schedulePayment != null ? schedulePayment.PaymentInstrumentType.ToString() : "";
                    achRecord.AchPauseStatus = loan.IsAutoPay ? "Yes" : "No";
                    achRecord.AccrualPauseMode = accrualInfo != null && accrualInfo.AccrualStop ? "Yes" : "No";
                    achRecord.DaySinceACHPaused = loan != null && loan.AutoPayHistory != null && loan.AutoPayHistory.Count > 0 && loan.AutoPayHistory.LastOrDefault().IsAutoPay == false ? (int)(TenantTime.Now.Date - loan.AutoPayHistory.LastOrDefault().StopDate.Time).TotalDays + 1 : 0;
                    achRecord.DaySinceAccrualPaused = accrualInfo != null && accrualInfo.AccrualStop == true ? (int)(TenantTime.Now.Date - accrualInfo.AccrualStopDate).TotalDays + 1 : 0;
                    achRecord.DPD = accrualInfo != null && accrualInfo.Due != null ? accrualInfo.Due.DPDDays : 0;
                    achRecord.TransactionDate = new TimeBucket(attempts.PaymentDate);
                    achRecord.PaymentScheduleDate = new TimeBucket(attempts.EffectiveDate);
                    achRecord.UserName = schedulePayment != null ? schedulePayment.InitiatedBy : "";
                    achRecord.ProductPortfolioType = loan.ProductPortfolioType.ToString();
                    achRecord.AchAmount = attempts.Principal + attempts.Interest + attempts.FeeAmount + attempts.PriorInterest;
                    achRecord.TransactionReferenceId = "";
                    if (schedulePayment != null && (schedulePayment.PaymentInstrumentType == PaymentInstrumentType.Ach))
                        achRecord.TransactionReferenceId = schedulePayment.ReferenceId;
                    if (schedulePayment != null && schedulePayment.PaymentInstrumentType == PaymentInstrumentType.Check)
                        achRecord.TransactionReferenceId = schedulePayment.CheckNo;

                    achReportData.Add(achRecord);

                }
                catch (System.Exception e)
                {
                    Logger.Error($"Error for loan in GetACHFailureReport {attempts.LoanNumber} : ", e);
                }

            }
            return achReportData;
        }

        public async Task<List<LoanEarningDetails>> GetEaringReport(LoanEarningRequest loanEarningRequest)
        {
            return await GetEarningReport(loanEarningRequest);
        }

        public async Task<List<LoanEarningDetails>> GetEarningReport(LoanEarningRequest loanEarningRequest)
        {
            if (loanEarningRequest == null)
            {
                throw new ArgumentException("Argument is null ", nameof(loanEarningRequest));
            }
            if (loanEarningRequest.ToDate < loanEarningRequest.FromDate)
            {
                throw new ArgumentException("To date should not be less then from date");
            }
            if(loanEarningRequest.FundedFromDate.HasValue || loanEarningRequest.FundedToDate.HasValue)
            {
                if(loanEarningRequest.FundedToDate.Value < loanEarningRequest.FundedFromDate.Value)
                {
                    throw new ArgumentException("Funded to date should not be less then from date");
                }
            }
            loanEarningRequest.ToDate = loanEarningRequest.ToDate.AddDays(1);
            var loanEarning = new LMS.LoanAccounting.LoanEarningRequest();
            loanEarning.FromDate = loanEarningRequest.FromDate;
            loanEarning.ToDate = loanEarningRequest.ToDate;
            loanEarning.FundedFromDate = loanEarningRequest.FundedFromDate.HasValue ? loanEarningRequest.FundedFromDate : null;
            loanEarning.FundedToDate = loanEarningRequest.FundedToDate.HasValue ? loanEarningRequest.FundedToDate : null;
            loanEarning.ProductId = loanEarningRequest.ProductId;
            var loanDetails = await AccrualService.GetPBOTDetailsByFundedDateRange(loanEarning);


            var distinctData = loanDetails.OrderByDescending(x => x.LoanNumber).Distinct().ToList();
            var funders = AchConfigService.GetFundingSources();
            var products = await ProductService.GetAll();
            var reportData = new List<LoanEarningDetails>();
            foreach (var item in distinctData)
            {
                try
                {
                    var loan = await LoanOnboardingService.GetLoanInformationByLoanNumber(item.LoanNumber);
                    var loanSchedule = await LoanOnboardingService.GetLoanSchedule(item.LoanNumber);
                    var accrualInfo = await AccrualService.GetLoanByScheduleVersion(item.LoanNumber, loanSchedule.ScheduleVersionNo);
                    var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow("loan", item.LoanNumber);
                    var scheduleTrekkingInfo = await AccrualService.GetScheduleByLoanNumberVersion(loan.LoanNumber, loanSchedule.ScheduleVersionNo);
                    var fees = await AccrualService.GetFees(item.LoanNumber);
                    if (loanEarningRequest.FundedFromDate != null && loanEarningRequest.FundedToDate != null && loan.LoanFundedDate.Time.Date >= loanEarningRequest.FundedFromDate.Value.Date && loan.LoanFundedDate.Time.Date <= loanEarningRequest.FundedToDate.Value.Date || loanEarningRequest.FundedFromDate == null && loanEarningRequest.FundedToDate == null)
                    {
                        var earnings = new LoanEarningDetails();
                        earnings.LoanNumber = item.LoanNumber;
                        earnings.LoanProductId = loan != null ? loan.LoanProductId : "";
                        earnings.StatusWorkFlowId = statusWorkFlow != null ? statusWorkFlow.StatusWorkFlowId : "";
                        earnings.BorrowerName = loan != null ? loan.PrimaryApplicantDetails.Name.First + " " + loan.PrimaryApplicantDetails.Name.Last : "";
                        earnings.FundedAmount = loanSchedule != null ? loanSchedule.FundedAmount : 0;
                        earnings.RepayAmount = loan != null && loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCA.ToString() || loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCALOC.ToString() ? loanSchedule.LoanAmount * loanSchedule.FactorRate : loanSchedule.LoanAmount * loanSchedule.AnnualRate;
                        earnings.FundedDate = loan != null ? loan.LoanFundedDate : null;
                        earnings.InstallmentAmount = loanSchedule != null && loanSchedule.ScheduleDetails != null ? loanSchedule.ScheduleDetails.FirstOrDefault().PaymentAmount : 0;
                        earnings.TotalNoOfPayments = loanSchedule != null && loanSchedule.ScheduleDetails != null ? loanSchedule.ScheduleDetails.Where(x => x.IsMissed == false).ToList().Count : 0;
                        earnings.TotalAmountReceived = accrualInfo != null && accrualInfo.PaymentInfo != null ? accrualInfo.PaymentInfo.CumulativeTotalPaid : 0;
                        var interestEarned = item.PBOTAccrualDetails != null ? item.PBOTAccrualDetails.Where(x => x.ProcessingDate >= loanEarningRequest.FromDate.Date && x.ProcessingDate <= loanEarningRequest.ToDate.Date).Sum(x=>x.InterestEarned) : 0;
                        var feeEarned = accrualInfo != null && accrualInfo.PaymentInfo != null ? accrualInfo.PaymentInfo.PaidFeeAmount : 0;
                        earnings.TotalEarning = Math.Round(interestEarned + feeEarned, 2);
                        earnings.InterestEarned = Math.Round(interestEarned, 2);
                        earnings.FeeEarned =  Math.Round(feeEarned, 2);
                        earnings.LoanStatus = statusWorkFlow != null ? statusWorkFlow.Name : "";
                        earnings.ProductPortfolioType = loan.ProductPortfolioType.ToString();
                        earnings.NumberOfPaymentMade = scheduleTrekkingInfo != null && scheduleTrekkingInfo.ScheduleTracking != null ? scheduleTrekkingInfo.ScheduleTracking.Where(x => x.IsPaid == true).Count() : 0;
                        earnings.PercentagePaidOff = loan != null && earnings.RepayAmount > 0 ?
                                                                  Math.Round((earnings.TotalAmountReceived / earnings.RepayAmount) * 100, 2) : 0;
                        earnings.NumberOfPaymentRemaining = earnings.TotalNoOfPayments - earnings.NumberOfPaymentMade;
                        earnings.TotalOutstandingAmount = accrualInfo != null && accrualInfo.PBOT != null ? accrualInfo.PBOT.TotalPrincipalOutStanding + accrualInfo.PBOT.TotalInterestOutstanding + accrualInfo.PBOT.TotalFeeOutstanding : 0;
                        earnings.PercentageOutstanding = 100 - earnings.PercentagePaidOff;
                        earnings.AutoPayPause = loan.IsAutoPay ? "Yes" : "No";
                        earnings.AccrualPause = accrualInfo != null && accrualInfo.AccrualStop ? "Yes" : "No";
                        earnings.LastPaymentReceivedDate = accrualInfo != null && accrualInfo.PaymentInfo != null ? new TimeBucket(accrualInfo.PaymentInfo.LastPaymentReceivedDate) : null;
                        earnings.LastPaymentReceivedAmount = accrualInfo != null && accrualInfo.PaymentInfo != null ? accrualInfo.PaymentInfo.LastPaymentAmount : 0;
                        earnings.DPD = accrualInfo != null && accrualInfo.Due != null ? accrualInfo.Due.DPDDays : 0;
                        earnings.InterestAccrued = accrualInfo != null && accrualInfo.PBOC != null ? accrualInfo.PBOC.CummulativeInterestAccured : 0;
                        earnings.InterestReceived = accrualInfo != null && accrualInfo.PaymentInfo != null && accrualInfo.Schedule != null ? accrualInfo.PaymentInfo.PaidInterestAmount + accrualInfo.PaymentInfo.PaidPriorInterest + accrualInfo.Schedule.PaidAdditionalInteret : 0;
                        earnings.TotalFeeReceived = accrualInfo != null && accrualInfo.PaymentInfo != null ? accrualInfo.PaymentInfo.PaidFeeAmount : 0;
                        earnings.BorrowerId = loan!= null ? loan.BorrowerId : string.Empty;
                        var tax = fees != null ? fees.Where(x=>x.IsPaid).Sum(x=>x.GSTAmount) : 0;
                        earnings.TotalAmountReceivedWithoutTax = Math.Round(earnings.TotalAmountReceived - tax, 2);
                        earnings.TotalFeesReceivedWithOrigination = fees != null ? fees.Where( x=> x.IsPaid).Sum(x=>x.PaidAmount) : 0;
                        earnings.Tenure = loanSchedule != null ? loanSchedule.Tenure : 0;
                        earnings.PaymentFrequency = loanSchedule != null ? loanSchedule.PaymentFrequency : PaymentFrequency.Monthly;
                        earnings.LoanAmount = loanSchedule != null ? loanSchedule.LoanAmount : 0;
                        earnings.TotalPrincipalReceived = accrualInfo != null && accrualInfo.PaymentInfo != null ? accrualInfo.PaymentInfo.PaidPrincipalAmount : 0;
                        earnings.TotalDrawdownAmount = loanSchedule != null ? loanSchedule.TotalDrawDown : 0;

                        if(loan != null && !string.IsNullOrEmpty(loan.FunderId) && funders != null && funders.Count > 0){
                            earnings.FunderId = loan.FunderId;
                            var filteredFunder =  funders.FirstOrDefault(x => x.Id == loan.FunderId);
                            if(filteredFunder != null){
                                earnings.FunderName = filteredFunder.Name;
                            }
                        }
                        if(loan != null && !string.IsNullOrEmpty(earnings.LoanProductId) && products != null && products.Count > 0){
                            var filteredProduct =  products.FirstOrDefault(x => x.ProductId == earnings.LoanProductId);
                            if(filteredProduct != null){
                                earnings.ProductName = filteredProduct.Name;
                            }
                        }
                        reportData.Add(earnings);
                    }
                }
                catch (System.Exception e)
                {
                    Logger.Error($"Error for loan in GetEaringReport {item} : ", e);
                }
            }

            return reportData;
        }

        public async Task<List<TDSReportDetails>> GetTDSReport(string entityType,string tagName)
        {
            if (string.IsNullOrWhiteSpace(entityType))
            {
                throw new ArgumentException("Argument is null ", nameof(entityType));
            }
            if (string.IsNullOrWhiteSpace(tagName))
            {
                throw new ArgumentException("Argument is null ", nameof(tagName));
            }
            var flagList = FlagService.GetAllFlagsByTag(entityType,tagName);
            var reportDetails = new List<TDSReportDetails> ();
            if (flagList == null || !flagList.Any())
            {
                return null;
            }

            var activeFlagList = flagList.Where(f => f.Status == FlagStatus.Active).OrderByDescending(f => f.FlagCreatedDate).GroupBy(f=>f.EntityId).Select(f => f.FirstOrDefault());

            foreach (var flagData in activeFlagList)
            {
                TDSReportDetails tdsreport = new TDSReportDetails();
                var loanDetails = await LoanOnboardingService.GetLoanInformationByLoanNumber(flagData.EntityId);
                if(loanDetails == null)
                {
                    throw new ArgumentException($"Loan details not found for {flagData.EntityId}");
                }
                var businessData = loanDetails.PrimaryBusinessDetails;
                if(businessData == null)
                {
                    throw new ArgumentException($"Business details not found for {loanDetails.LoanNumber}");
                }
                var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow(entityType, loanDetails.LoanNumber);
                tdsreport.LoanNumber = loanDetails.LoanNumber;
                tdsreport.LoanProductId = loanDetails.LoanProductId;
                tdsreport.ProductPortfolioType = loanDetails.ProductPortfolioType.ToString();
                tdsreport.StatusWorkFlowId = statusWorkFlow != null ? statusWorkFlow.StatusWorkFlowId : "";
                tdsreport.BorrowerId = loanDetails.BorrowerId;
                tdsreport.BorrowerName = businessData.BusinessName;
                tdsreport.BorrowerEmail = businessData.Email.EmailAddress;
                tdsreport.BorrowerPhone = businessData.Phone.PhoneNo;
                tdsreport.FlagName = tagName;
                tdsreport.FlagAddedDate = new TimeBucket(flagData.FlagCreatedDate);
                tdsreport.FlagAddedBy = flagData.UserName;
                reportDetails.Add(tdsreport);
            }
            return reportDetails;
        }

        #endregion PublicMethods

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
            {
                throw new ArgumentException($"{nameof(entityType)} is mandatory");
            }

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
            {
                throw new ArgumentException($"{entityType} is not a valid entity");
            }

            return entityType;
        }

        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (System.Exception exception)
            {
                throw new System.Exception("Unable to de-serialize:" + response, exception);
            }
        }

        private DateTimeOffset GetPreviousBusinessDay(DateTimeOffset scheduleDate)
        {
            DateTimeOffset processingDate = scheduleDate;
            for (int i = 0; i < FundingConfiguration.InstructionCreationDays; i++)
            {
                var todayInfo = CalendarService.GetDate(processingDate.Year, processingDate.Month, processingDate.Day);
                processingDate = todayInfo.PreviousBusinessDay.Date;
            }
            return processingDate;
        }

        private DateTimeOffset GetNextRetryDay(DateTimeOffset scheduleDate, int days)
        {
            DateTimeOffset processingDate = scheduleDate;
            for (int i = 0; i < days; i++)
            {
                var todayInfo = CalendarService.GetDate(processingDate.Year, processingDate.Month, processingDate.Day);
                processingDate = todayInfo.NextBusinessDay.Date;
            }
            return processingDate;
        }
        
        /* Check for both cases in object for instruction property */
        private static Instruction GetInstruction(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.Instruction;
            if (!HasProperty(resultProperty))
            {
                throw new ArgumentNullException("instruction property not found in payload");
            } 

            var instruction = GetValue(resultProperty);
            if (instruction == null)
            {
                resultProperty = () => data.instruction;
                if (!HasProperty(resultProperty))
                {
                    throw new ArgumentNullException("instruction property not found in payload");
                } 

                instruction = GetValue(resultProperty);
                if(instruction == null)
                {
                    throw new ArgumentException("Argument is null or whitespace email");
                }
            }
            var instructionDetail = JsonConvert.DeserializeObject<Instruction>(instruction.ToString());
            return instructionDetail;

        }
        
        /* Check for both cases in object for file id property */
        private static string GetFileId(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.FileId;
            dynamic fileId = null;
            if(HasProperty(resultProperty))
            {
                fileId = GetValue(resultProperty);
                if(fileId == null)
                {
                    resultProperty = () => data.fileId;
                    if(HasProperty(resultProperty))
                    {
                        fileId = GetValue(resultProperty);
                    }
                }
            }
            return fileId;
        }

        /* Check for both cases in object for description property */
        private static string GetNOAReasonValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.Description;
            dynamic description = null;
            if(HasProperty(resultProperty))
            {
                description = GetValue(resultProperty);
                if(description == null)
                {
                    resultProperty = () => data.description;
                    if(HasProperty(resultProperty))
                    {
                        description = GetValue(resultProperty);
                    }
                }
            }
            return description;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

        private string EnsureCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            return username;
        }

        private async Task<List<ILoanPaymentAttempts>> GetAllAttemptData(string loanNumber)
        {

            if (string.IsNullOrEmpty(loanNumber))
                throw new ArgumentException($"{nameof(loanNumber)} is mandatory. Please enter a valid loanNumber.");

            var fundingAttemptData = await FundingRequestAttemptsRepository.GetAllAttemptData(loanNumber);

            return fundingAttemptData;
        }

        /* Check for both cases in object for reason property */
        private static string GetReasonValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.Reason;
            dynamic reason = null;
            if(HasProperty(resultProperty))
            {
                reason = GetValue(resultProperty);
                if(reason == null)
                {
                    resultProperty = () => data.reason;
                    if(HasProperty(resultProperty))
                    {
                        reason = GetValue(resultProperty);
                    }
                }
            }
            return reason;
        }

        private async Task<IFunding> AddFundingDetails(string entityType, string entityId, IFundingRequest request)
        {
            var fundingData = await AddFundingRequest(entityType, entityId, request);
            if (request.PaymentInstrumentType != PaymentInstrumentType.Ach)
            {
                await AddFundingRequestAttempts(entityType, entityId, new PaymentRequestAttempts
                {
                    ReferenceId = fundingData.ReferenceId,
                    AttemptDate = TenantTime.Now,
                    FundingRequestAttemptId = Guid.NewGuid().ToString("N"),
                    FundingRequestID = fundingData.Id,
                    LoanNumber = fundingData.LoanNumber,
                    BankAccountNumber = request.BankAccountNumber,
                    BankAccountType = request.BankAccountType,
                    BankRTN = request.BankRTN,
                    BorrowersName = request.BorrowersName,
                    ProviderSubmissionDate = TenantTime.Now,
                    ProviderResponseStatus = LookupService.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key,
                    AttemptStatus = LookupService.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key
                });
            }
            return fundingData;
        }

        private SftpClient GetSftpClient()
        {
            return new SftpClient(FundingConfiguration.Provider.Host, FundingConfiguration.Provider.Port, FundingConfiguration.Provider.Username, FundingConfiguration.Provider.Password);
        }

        private void UploadFile(Stream content)
        {
            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);
                try
                {
                    FaultRetry.RunWithAlwaysRetry(
                        () =>
                        {
                            client.UploadFile(content, FundingConfiguration.FailedLocation + FundingConfiguration.FailedFileName + "_"+ TenantTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv", true);
                        });
                }
                catch (Exception exception)
                {
                    Logger.Error("Unable to upload file", exception);
                } 
            }
        }

        private StringBuilder CreateFailedProcessingFile(List<PaymentRejectedDetail> failures, List<string> headers)
        {
            if(failures == null || failures.Count() <= 0)
            {
                return null;
            }
            var csv = new StringBuilder();
            headers.Add("Exception");
            var csvHeader = string.Join(",", headers);
            csv.AppendLine(csvHeader );
            foreach (var item in failures)
            {
                var row = string.Empty;
                foreach (var header in headers)
                {
                    if(!string.IsNullOrWhiteSpace(row))
                    {
                        row += ",";
                    }
                    row += GetPropValue(item, header);  
                }
                csv.AppendLine(row);
            }
            return csv;
        }

        private static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

    }
}