﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductConfiguration;
using LMS.Foundation.Calculus;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;

namespace LMS.Payment.Processor
{
    public partial class PaymentProcessorService
    {
        #region Public Methods

        public async Task<IPayment> AddManualPayment(string entityType, string entityId,
            IFundingRequest request)
        {
            return await AddManualPayment(entityType, entityId, request, new List<string>());
        }

        /// <summary>
        /// This method is deprecated and will be removed later
        /// It will be replaced with AddManualPayments
        /// Currently both are retained for backward compatibilty.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <param name="loanPortfolioTypes"></param>
        /// <returns></returns>
        /// <exception cref="InvalidArgumentException"></exception>
        public async Task<IPayment> AddManualPayment(string entityType, string entityId,
            IFundingRequest request, List<string> loanPortfolioTypes)
        {
            switch (entityType)
            {
                case "loan":
                    return await AddManualPaymentForOneLoan(entityType, entityId, request);
                case "borrower":
                    
                    if (!IsBorrowerValid(entityId))
                    {
                        throw new InvalidArgumentException(nameof(entityId));
                    }                    

                    ValidatePortfolioTypes(loanPortfolioTypes);
                    var loanIds = await GetListOfOpenLoansForBorrowerSortedByOpenDate(entityId, loanPortfolioTypes);
                    var loansAndAmounts = await GetCollectionAmountsPerLoan(loanIds,
                        request.PaymentAmount);
                    var payments = new List<IPayment>();
                    foreach (var (loanId, paymentAmount) in loansAndAmounts)
                    {
                        // var payload =
                        //     CreatePayloadForManualPaymentOfOneLoan(loanId, paymentAmount, request);
                        request.PaymentAmount = paymentAmount;
                        var payment = await AddManualPaymentForOneLoan("loan", loanId, request);
                        payments.Add(payment);
                    }

                    // if all payments are to be returned, use AddManualPayments (/manual/payments/
                    // this method is retained for backward compatibilty but will be removed later
                    // new enhancments will be done only to AddManualPayments and not to AddManualPayment
                    return payments.LastOrDefault();
                default:
                    throw new InvalidArgumentException(nameof(entityType));
            }
        }
        
        /// <summary>
        /// Enhancement to AddManualPayments - return value is a list rather than single object
        /// Subsequently, AddManualPayment will be removed and only AddManualPayment will be maintained and enhanced.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <param name="loanPortfolioTypes"></param>
        /// <returns></returns>
        /// <exception cref="InvalidArgumentException"></exception>
        public async Task<List<IPayment>> AddManualPayments(string entityType, string entityId,
            IFundingRequest request, List<string> loanPortfolioTypes)
        {
            var payments = new List<IPayment>();
            switch (entityType)
            {
                case "loan":
                    payments.Add(await AddManualPaymentForOneLoan(entityType, entityId, request));
                    return payments;
                case "borrower":
                    if (!IsBorrowerValid(entityId))
                    {
                        throw new InvalidArgumentException(nameof(entityId));
                    }

                    ValidatePortfolioTypes(loanPortfolioTypes);
                    var loanIds = await GetListOfOpenLoansForBorrowerSortedByOpenDate(entityId, loanPortfolioTypes);
                    Logger.Info("List of Open Loans", new {entityType, entityId, request, loanPortfolioTypes, loanIds});
                    var loansAndAmounts = await GetCollectionAmountsPerLoan(loanIds,
                        request.PaymentAmount);
                    if (!loansAndAmounts.Any())
                    {
                        throw new NotFoundException($"None of the active loans [{string.Join(",", loanIds)}] for borrowerId" +
                                                    $"[{entityId}], portfolio type" +
                                                    $"[{string.Join(",", loanPortfolioTypes)}] have any outstanding," +
                                                    $"hence payment is not processed");
                    }
                    Logger.Info("List of Loans and Amounts",
                        new { entityType, entityId, request, loanPortfolioTypes, loansAndAmounts });
                    foreach (var (loanId, paymentAmount) in loansAndAmounts)
                    {
                        // var payload =
                        //    CreatePayloadForManualPaymentOfOneLoan(loanId, paymentAmount, request);
                        request.PaymentAmount = paymentAmount;
                        var payment = await AddManualPaymentForOneLoan("loan", loanId, request);
                        payments.Add(payment);
                    }

                    return payments;
                default:
                    throw new InvalidArgumentException(nameof(entityType));
            }
        }
        
        #endregion Public Methods
        
        #region Private Methods

        private IPaymentReceivedRequest CreatePayloadForManualPaymentOfOneLoan(string loanId, double paymentAmount,
            IFundingRequest request)
        {
            
            // TODO the payload for one loan payment is currently in navigator: 
            // TODO ./app/components/lendfoundry/loan-actions/manual-payment.html
            // TODO for paymentTypeId = Cash, Check, Ach or Others, the payload is create in a different manner
            // TODO this can be used as a guide to create the payload for passed loanId and paymentAmount
            
            // TODO simplest way would be to just modify the loanId and paymentAmount in the original payload
            // TODO if so we may not even need this method
            
            return new PaymentReceivedRequest();
            
        }

        private async Task<IPayment> AddManualPaymentForOneLoan(string entityType, string entityId,
            IFundingRequest request)
        {
            request.PaymentType = PaymentType.Manual;
            request.Status = request.PaymentInstrumentType != PaymentInstrumentType.Ach
                ? LookupService.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key
                : LookupService.GetLookupEntry("requeststatus", "initiated").FirstOrDefault().Key;
            request.ActualAccountDate = TenantTime.Now;
            if (request.PaymentAmount <= 0.01)
            {
                request.PaymentAmount = request.InterestAmount + request.AdditionalInterestAmount +
                                        request.PrincipalAmount + request.Fees.Sum(x => x.FeeAmount);
                if (request.PaymentAmount < 0.01)
                {
                    throw new InvalidArgumentException("Payment amount must be greater than zero");
                }
            }

            if (request.PaymentPurpose.ToLower() == "payoff" && request.DiscountAmount > 0)
            {
                IPayOffFeeDiscount payoffDiscount = new PayOffFeeDiscount();
                payoffDiscount.LoanNumber = entityId;
                payoffDiscount.PayOffFeeScheduleDetails = new List<IPayOffFeeScheduleDetails>();
                var feeDiscount = new PayOffFeeScheduleDetails
                {
                    PayOffFromDate = request.PaymentScheduleDate.AddDays(-1).Date,
                    PayOffToDate = request.PaymentScheduleDate.AddDays(1).Date,
                    Amount = request.DiscountAmount
                };
                payoffDiscount.PayOffFeeScheduleDetails.Add(feeDiscount);

                // TODO 1 see TODO 4 below
                await LoanOnboardingService.AddPayOffDiscount(payoffDiscount);
            }
            if (request.PaymentInstrumentType == PaymentInstrumentType.TDS)
            {
                var activeSchedule = await LoanOnboardingService.GetLoanSchedule(entityId);
                if (activeSchedule == null)
                {
                    throw new ArgumentException($"No out standing balances for {entityId}");                    
                }
                var loanSchedule = await AccrualService.GetScheduleByLoanNumberVersion(entityId,activeSchedule?.ScheduleVersionNo);

                if(loanSchedule == null)
                {
                    throw new ArgumentException($"TDS not applicable");                    
                }
                var lastUnpaidSchedule = loanSchedule.ScheduleTracking.LastOrDefault();
                var paidInterest = loanSchedule.ScheduleTracking.Where(i => i.Installmentnumber == lastUnpaidSchedule.Installmentnumber &&i.IsPaid == false).Sum(i   => i.InterestAmount);
                var unpaidInterest =  RoundOff.Round(activeSchedule.ScheduleDetails.Where(i => i.Installmentnumber == lastUnpaidSchedule.Installmentnumber) .FirstOrDefault().InterestAmount - paidInterest,2,"mathematical");                
                if(request.InterestAmount > unpaidInterest)
                {
                    throw new ArgumentException($"TDS amount cannot be more then Unpaid Interest");
                }
            }

            IPaymentReceivedRequest payment = new PaymentReceivedRequest();
            payment.ProcessingDate = request.PaymentScheduleDate;
            payment.EffectiveDate = request.EffectiveDate.HasValue ? request.EffectiveDate.Value :
                request.PaymentScheduleDate;
            payment.TotalAmount = request.PaymentAmount;
            payment.Principal = request.PrincipalAmount;
            payment.Interest = request.InterestAmount;
            payment.AdditionalInterest = request.AdditionalInterestAmount;
            payment.Action = request.PaymentPurpose;
            request.TransactionType = TransactionType.Credit;
            payment.TDSCertificateNumber = request.TDSCertificateNumber;
            payment.DocumentId = request.DocumentId;
            payment.PriorInterest = request.PriorInterestAmount;


            payment.Fees = new List<LoanAccounting.IFee>();
            if (request.Fees != null)
            {
                foreach (var fee in request.Fees)
                {
                    LoanAccounting.IFee feeDetails = new LoanAccounting.Fee();
                    feeDetails.FeeAmount = fee.FeeAmount;
                    feeDetails.FeeName = fee.FeeName;
                    feeDetails.AppliedOnDate = TenantTime.Now.Date;
                    feeDetails.Id = fee.Id;
                    payment.Fees.Add(feeDetails);
                }
            }

            payment.FeeAmount = payment.Fees.Sum(x => x.FeeAmount);
            payment.Description = "Manual Payment";

            // TODO 2 see TODO 4 below
            var paymentDetails = await AccrualService.ApplyPaymentWithTotalAmount(entityId, payment);
            // TODO 3 see TODO 4 below
            var fundingData = await AddFundingDetails(entityType, entityId, request);

            // TODO 4 there are 3 places where database is impacted and they are all async, one is internal and the 
            // TODO other 2 are external. Instead these calls need to be within a database transaction
            // TODO Otherwise the data consistency cannot be guaranteed after this operation

            await EventHubClient.Publish("PaymentApplied", new
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = fundingData
            });

            return paymentDetails;
        }

        /// <summary>
        /// This is needed to be able to provide the caller with the right information in case no processing happens
        /// Then the caller will know whether it was because of a non existent borrower id, or that the borrower id
        /// was correct, but that borrower did not have any open loans.
        /// This does not impact the functionality in any way, so in the interest of time this is not implemented.
        /// </summary>
        /// <param name="borrowerId"></param>
        /// <returns></returns>
        private static bool IsBorrowerValid(string borrowerId)
        {
            return true;
        }

        private static void ValidatePortfolioTypes(IEnumerable<string> loanPortfolioTypes)
        {
            foreach (var loanPortfolioType in loanPortfolioTypes)
            {
                if (!Enum.TryParse(loanPortfolioType, true, out PortfolioType _))
                {                    
                    throw new InvalidEnumArgumentException(
                        $"{loanPortfolioType} is not valid portfolio type. Allowed values are" +
                        $"{string.Join(",",Enum.GetNames(typeof(PortfolioType)))} .");
                }
            }
        }

        private async Task<List<string>> GetListOfOpenLoansForBorrowerSortedByOpenDate(string borrowerId,
            List<string> portfolioTypes)
        {
            var borrowerAllLoans = await LoanFilterService.GetByBorrowerId(borrowerId);
            borrowerAllLoans = borrowerAllLoans.Where(l => l.StatusCode == GetInProgressStatusCode()).ToList();

            if (!borrowerAllLoans.Any())
            {
                return new List<string>();
            }
            Logger.Info("List of loans step 1 ", new {borrowerId, portfolioTypes, borrowerAllLoans});


            if (portfolioTypes != null && portfolioTypes.Any())
            {
                borrowerAllLoans = borrowerAllLoans.Where(l =>
                    portfolioTypes.FindIndex((p) => 
                        p.Equals(l.ProductPortfolioType, StringComparison.OrdinalIgnoreCase)
                     ) != -1).ToList();
            }
            
            Logger.Info("List of loans step 2 ", new {borrowerId, portfolioTypes, borrowerAllLoans});

            var loanNumbers = borrowerAllLoans.OrderBy(l=>l.LoanFundedDate.Time).Select(r => r.LoanNumber).ToList();

            Logger.Info("List of loans step 3 ", new {borrowerId, portfolioTypes, loanNumbers});

            return loanNumbers;
        }

        private async Task<IEnumerable<(string, double)>> GetCollectionAmountsPerLoan(IEnumerable<string> loanIds,
            double collectionAmount)
        {
            var amountToApportion = collectionAmount;

            var collectionAmounts = new List<(string loanId, double apportionedAmount, double payOffAmount)>();
            var finalCollectionAmounts = new List<(string loanId, double apportionedAmount)>();

            // TODO: for efficiency, we may need to have endpoint to return schedule for multiple loan
            foreach (var loanId in loanIds)
            {
                Logger.Info("Getting Amount for Loan", new {loanIds, collectionAmounts, loanId});
                var activeSchedule = await LoanOnboardingService.GetLoanSchedule(loanId);
                if (activeSchedule.ScheduleVersionNo == null)
                {
                    throw new Exception($"Loan Schedule not found for loanId {loanId}");
                }

                var loanSchedule =
                    await AccrualService.GetLoanByScheduleVersion(loanId, activeSchedule.ScheduleVersionNo);

                // TODO get payoff amount for this loan
                // TODO Is amountToApportion >= payoff amount?
                // TODO Yes : add payoff flag = true
                // TODO No : existing logic
                
                // TODO There is a special case where PBOC and Schedule may both have the amount for
                // TODO certain days. For those cases we may have to subtract some amounts.
                // TODO Paragi will get back on what those conditions are.
                // TODO Until then, we may be over collecting some due amounts but not under collecting.
                //NOTE as per LFPROJ-1196 and discussion with Shubha only PBOC needs to collect first
                var currentOutstanding = loanSchedule.PBOC.CurrentPrincipalOutStanding +
                                         loanSchedule.PBOC.CurrentInterestOutStanding;
                
                var currentOutstandingForPayOff = loanSchedule.PBOT.TotalInterestOutstanding +
                                                  loanSchedule.PBOT.TotalPrincipalOutStanding +
                                                  loanSchedule.PBOT.TotalFeeOutstanding;
                
                Logger.Info("Current outstanding", new { loanId, loanSchedule.PBOC.CurrentPrincipalOutStanding,
                    loanSchedule.PBOC.CurrentInterestOutStanding, loanSchedule.Schedule.SchedulePrincipal,
                    loanSchedule.Schedule.ScheduleInterest, loanSchedule.Schedule.AdditionalInterest});

                if (currentOutstanding >= 0.01)
                {
                    // take the minimum of available amount  and outstanding to apportion for this loan
                    var amountForThisLoan = currentOutstanding < amountToApportion ? currentOutstanding : amountToApportion;
                    
                    if (currentOutstandingForPayOff <= amountForThisLoan)
                    {
                        amountForThisLoan = currentOutstandingForPayOff;
                    }
                    
                    collectionAmounts.Add((loanId, amountForThisLoan,currentOutstandingForPayOff));
                    amountToApportion -= amountForThisLoan;
                }
                else
                {
                    collectionAmounts.Add((loanId, 0,currentOutstandingForPayOff));
                }
                Logger.Info("Amount left to apportion", new { loanIds, collectionAmounts, loanId, amountToApportion });

                // avoid getting schedule for next loans if the amount is already exhausted
                if (amountToApportion <= 0.01)
                {
                    break;
                }
            }

            foreach (var loanPayOff in collectionAmounts)
            {
                var apportionedAmount = loanPayOff.apportionedAmount;
                if (amountToApportion <= 0.01)
                {
                    finalCollectionAmounts.Add((loanPayOff.loanId,apportionedAmount));
                }
                else
                {
                    var payOffAmountForThisLoan = loanPayOff.payOffAmount - apportionedAmount;
                    var additionalAmountForThisLoan = payOffAmountForThisLoan < amountToApportion ? payOffAmountForThisLoan : amountToApportion;
                    apportionedAmount = apportionedAmount + additionalAmountForThisLoan;
                    finalCollectionAmounts.Add((loanPayOff.loanId,apportionedAmount));
                    amountToApportion -= additionalAmountForThisLoan;
                }

            }

            // add leftover amount to the first loan
            if (finalCollectionAmounts.Any())
            {
                // TODO instead of this logic attach to the first loan where payoff is false
                // TODO if there is no loan where payoff is false but still we have excess, where does the money go?
                //NOTE: It will goto last loan as per comment from Shubha
                var lastIndex = finalCollectionAmounts.Count - 1;
                finalCollectionAmounts[lastIndex] = (finalCollectionAmounts[lastIndex].loanId,
                    finalCollectionAmounts[lastIndex].apportionedAmount + amountToApportion);
            }
            return finalCollectionAmounts;
        }

        private static string GetInProgressStatusCode()
        {
        
            // TODO replace with a fetch from configuration
            return "200.20";
        }
        
        #endregion Private Methods

    }
}
