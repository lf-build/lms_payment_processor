using System;

namespace LMS.Payment.Processor
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}