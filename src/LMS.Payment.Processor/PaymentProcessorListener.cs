﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace LMS.Payment.Processor
{
    public class PaymentProcessorListener : IPaymentProcessorListener
    {
        #region Constructor
        public PaymentProcessorListener
        (
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            IPaymentProcessorServiceFactory paymentProcessorServiceFactory,
            IConfigurationServiceFactory configurationFactory)
        {

            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(paymentProcessorServiceFactory), paymentProcessorServiceFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);

            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            PaymentProcessorServiceFactory = paymentProcessorServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
        }
        #endregion

        #region Private Properties       
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IPaymentProcessorServiceFactory PaymentProcessorServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        #endregion


        #region Public Methods

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {

                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.

                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);
                    var reader = new StaticTokenReader(token.Value);
                    var eventHub = EventHubFactory.Create(reader);
                    var configuration = ConfigurationFactory.Create<FundingConfiguration>(Settings.ServiceName, reader).Get();
                    if (configuration != null && configuration.events != null)
                    {
                        var paymentProcessorService = PaymentProcessorServiceFactory.Create(reader, TokenHandler, logger);
                        configuration
                       .events
                       .ToList().ForEach(events =>
                       {
                           eventHub.On(events.Name, ProcessEvent(events, logger, paymentProcessorService));
                           logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                       });
                        eventHub.StartAsync();
                        logger.Info("-------------------------------------------");
                    }
                    else
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    
                });
                logger.Info("Payment Processor listener started");
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process Payment Processor listener", ex);
                logger.Info("\n Payment Processor listener  is working yet and waiting new event\n");
            }
        }

        #endregion

        #region Private Methods

        #region Process Event
        private Action<LendFoundry.EventHub.EventInfo> ProcessEvent(
           EventConfiguration eventConfiguration,
           ILogger logger,
           IPaymentProcessorService paymentService
           )
        {
            return async @event =>
            {
                string responseData = string.Empty;
                object data = null;

                try
                {
                    try
                    {
                        responseData = eventConfiguration.Response.FormatWith(@event); //posted data
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
                    }
                    catch { }

                    //    var entityId = eventConfiguration.EntityId.FormatWith(@event);
                    //   var entityType = eventConfiguration.EntityType.FormatWith(@event);
                    var Name = eventConfiguration.Name.FormatWith(@event);

                    var methodName = eventConfiguration.MethodToExecute;
                    MethodInfo method = typeof(IPaymentProcessorService).GetMethod(methodName);
                    object[] param = { data };
                    await (dynamic)method.Invoke(paymentService, param);


                    logger.Info($"Response Data : #{data}");
                    logger.Info($"Payment Processor event {Name} completed ");

                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while listening event {@event.Name}", ex);
                }
            };
        }
        #endregion

        #region Ensure Parameter
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
        #endregion

        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion

        #endregion

    }
}
